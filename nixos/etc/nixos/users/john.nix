# john.nix
#
# Defines a user account


{ config, lib, pkgs, ... }:

{

  # Don't forget to set a password with 'passwd'.
  users.users.john = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]
     ++ ( if ( config.networking.networkmanager.enable ) then [ "networkmanager" ] else [ ] )
     ++ ( if ( config.programs.light.enable ) then [ "video" ] else [ ] );

    createHome = true;
    home = "/home/john";
    shell = lib.mkIf ( config.programs.zsh.enable ) pkgs.zsh;
  };

}
