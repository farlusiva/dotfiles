# printing.nix
#
# This sets up printing

{ pkgs, ... }:

{
  services.printing = {
    enable = true;
    drivers = with pkgs; [
      gutenprint
      #gutenprintBin
      #hplip
      #postscript-lexmark
      #samsung-unified-linux-driver
      #splix
      #brlaser
      #brgenml1lpr
      #brgenml1cupswrapper
      #cnijfilter2
    ];
  };

  services.avahi = {
    enable = true;
    nssmdns4 = true;
  };

  # Enable samba
  services.samba.enable = true;

  environment.systemPackages = with pkgs; [
    cifs-utils # samba
    system-config-printer # Set samba passwords with this, if needed.
    #ghostscript
  ];
}
