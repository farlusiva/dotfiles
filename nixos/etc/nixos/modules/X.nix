# X.nix
#
# This module does everything to set up X the way I like it.

{ pkgs, ... }:

{
  # We need tamzen as some programs use it (st, qutebrowser, dmenu)
  imports = [
    ./audio.nix
    ./fonts.nix
  ];

  # Hardware things that X needs or wants
  hardware.opengl.driSupport = true;
  # from https://nixos.wiki/wiki/Accelerated_Video_Playback
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
      vaapiVdpau
      libvdpau-va-gl
    ];
  };

  # Enable X
  services.xserver = {
    enable = true;
    xkb.layout = "no";
    displayManager.startx.enable = true;
  };

  services.libinput.enable = true; # Enable touchpad support

  # Install slock and set up the setuid wrapper for it.
  programs.slock.enable = true;

  # Enable redshift
  location.provider = "geoclue2";
  services.redshift.enable = true;

  # Install the minimal amount of packages to get X working as I want it
  environment.systemPackages = with pkgs; [

    # Programs required for the nice X installation
    bspwm sxhkd
    lemonbar-xft

    # X utilites
    j4-dmenu-desktop feh dmenu
    xorg.xev xorg.xkill
    unclutter-xfixes pavucontrol
    flameshot # qt5
    dunst libnotify

    # Used by various scripts
    xdo xdotool xclip xorg.xmodmap

    # ricing
    pywal
    pastel # colour program

    # Patch st with a patch that combines the config.h and other patches
    # applied, all in one patch that doesn't conflict with anything.
    (pkgs.st.override (attrs: {
      patches = attrs.patches ++ [ /etc/nixos/st-patch.diff ];
    }))
  ];
}
