{ pkgs, lib, ... }:

{
  boot.kernel.sysctl = {
    "kernel.sysrq" = 1; # Enable sysrq
  };
}
