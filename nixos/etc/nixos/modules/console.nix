# console.nix
# This makes the console look nice.
#

{ pkgs, config, lib, ... }:

{

  # Import the tamzen font
  imports = [
    ./fonts.nix
  ];

  console.font = lib.mkForce
    (if (config.networking.hostName == "hestia") then
      "${pkgs.tamzen}/share/consolefonts/TamzenForPowerline10x20.psf"
    else
      "${pkgs.tamzen}/share/consolefonts/TamzenForPowerline8x15.psf"
    );

  # Switch fonts early to make the booting process look nicer.
  console.earlySetup = true;

  # Fetched from X/.Xresources
  console.colors = [
#    "2e3436" # the actual colour 0
    "000000" "cc0000" "4e9a06" "c4a000"
    "3465A4" "75507b" "06989a" "d3d7cf"
    "555753" "ef2929" "8ae234" "fce94f"
    "729fcf" "ad7fa8" "34e2e2" "eeeeec"
  ];

  # Have a nicer greeter.
  environment.etc.issue = lib.mkForce {
    text = ''

      NixOS ${config.system.nixos.release} (${config.system.nixos.codeName}) on kernel \r (\l)

    '';
    target = "issue";
  };

}
