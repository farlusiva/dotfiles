# audio.nix
#
# Mostly taken from https://nixos.wiki/wiki/PipeWire

{ lib, pkgs, ... }:

{

  # Here's what a non-pipewire config would look like:
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Pipewire config

  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  # Can be done differently after 24.05 as per the wiki
  environment.etc = {
      "wireplumber/bluetooth.lua.d/51-bluez-config.lua".text = ''
          bluez_monitor.properties = {
              ["bluez5.enable-sbc-xq"] = true,
              ["bluez5.enable-msbc"] = true,
              ["bluez5.enable-hw-volume"] = true,
              ["bluez5.headset-roles"] = "[ hsp_hs hsp_ag hfp_hf hfp_ag ]"
          }
      '';
  };

  environment.systemPackages = with pkgs; [
    pulseaudio # Need pactl for lemons.sh and volume_script.sh
  ];

}
