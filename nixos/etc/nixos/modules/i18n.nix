# i18n.nix
#
# Sets options regarding internationalization.


{
  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";
  console.keyMap = "no-latin1";

  # Set your time zone.
  time.timeZone = "Europe/Oslo";
}
