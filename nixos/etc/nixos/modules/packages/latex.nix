# latex.nix
#
# This module handles installing LaTeX.

{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    pgfplots

    # Install texlive and some package collections
    (texlive.combine {
      inherit (texlive)
      scheme-medium

      # The following are defined in
      # 'nixpkgs/pkgs/tools/typesetting/tex/texlive/pkgs.nix', which to the
      # best of my knowledge are not documented.
      collection-bibtexextra
      collection-latex
      collection-latexextra
      collection-latexrecommended
      collection-mathscience
      collection-pictures

      opensans # for a certain beamer theme
      tikz-cd
      quiver
      ;
    })

  ];
}
