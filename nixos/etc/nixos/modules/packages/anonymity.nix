# anonymity.nix

{ config, lib, pkgs, ... }:

let

  # Run mk_nix_channels.sh
  unstable = import <unstable> {};

in
{
  # Showcase on how this could be done with mkMerge and mkIf.
  config = lib.mkMerge [
    { environment.systemPackages = with pkgs; [

      gnupg
      tor # Starting the daemon with tor(1) is a prerequesite for using torsocks
      torsocks # test: 'torsocks curl ifconfig.me'
      spectre-cli # Formerly 'Master Password App'
    ]; }

    (lib.mkIf config.services.xserver.enable {
      environment.systemPackages = with pkgs; [
        tor-browser-bundle-bin
      ];
    })
  ];
}
