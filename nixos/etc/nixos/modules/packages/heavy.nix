# heavy.nix
#
# Heavy graphical programs that are too large or too seldom used to be placed in
# graphical.nix.
#

{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    kdenlive
    audacity
    gimp
  ];
}
