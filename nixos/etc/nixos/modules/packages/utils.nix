# utils.nix
#
# Common utilities

{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [

    # The necessary
    curl wget
    git stow
    coreutils bash
    gawk gnused gnutar
    gnumake sudo
    units unzip killall
    bc
    jshon
    lsof
    exiftool
    fzf
    fd
    zip
    psmisc # see what hinders unmounting a fuse filesystem with fuse(1)
    caffeine-ng # dmenu_sysutils.sh
    screenkey # dmenu_sysutils.sh
    xcolor # colour grabber
    playerctl
    ripgrep
    ripgrep-all # TODO: Try rga-fzf, a part of this
    delta # alternative pager for git
    mons

    lm_sensors

    # Syncthing
    syncthing
    syncthingtray # also gives syncthingctl

    # Don't have busybox symlink all its applets. It causes many namespace
    # collisions. See https://github.com/NixOS/nixpkgs/issues/10716
    (pkgs.busybox.override {
      extraConfig = ''
      CONFIG_INSTALL_APPLET_DONT y
      CONFIG_INSTALL_APPLET_SYMLINKS n
      '';
    })

    # Hardware-related packages, including drivers
    ntfs3g
    usbutils
    gparted

  ];
}
