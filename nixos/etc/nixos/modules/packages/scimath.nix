# scimath.nix
#
# Packages related to math/science

{ pkgs, ... }:

let
  # Run mk_nix_channels.sh
  unstable = import <unstable> {};
in

{
  environment.systemPackages = with pkgs; [
    #geogebra6
    #octave
    cantor
    sage
  ];
}
