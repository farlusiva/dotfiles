# theme.nix
#
# Installs the breeze theme. Configuration is left to the user (see 'breeze').
#
#

{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    breeze-icons
    breeze-qt5
    libsForQt5.breeze-gtk # previously 'gnome-breeze'
  ];

  # Replacement for stowing 'breeze', as NixOS and Arch recognize different
  # theme-name strings. The theme name &c can be found with lxappearance.
  # See also https://nixos.wiki/wiki/CDE.
  environment.etc."xdg/gtk-2.0/gtkrc".text = ''
    gtk-theme-name = Breeze-dark
    gtk-icon-theme-name = breeze-dark
  '';

  environment.etc."xdg/gtk-3.0/settings.ini".text = ''
    [Settings]
    gtk-theme-name = Breeze-dark
    gtk-icon-theme-name = breeze-dark
    gtk-application-prefer-dark-theme = true
  '';

}
