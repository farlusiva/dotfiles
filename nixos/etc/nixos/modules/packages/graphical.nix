# graphical.nix
#
# This module installs graphical programs that are not stricly required
# to get X working the way I like it.

{ pkgs, ... }:

let
   unstable = import <unstable> {};
in
{
  # Graphical programs. Here are full-fledged *programs*, not
  # things that are required to get a nice X setup, as
  # those are in X.nix.
  environment.systemPackages = with pkgs; [

    rxvt-unicode
    zathura picom udiskie
    qutebrowser # qutebrowser
    firefox
    unstable.firefox-devedition-bin
    element-desktop
    sioyek # Experimental

    filezilla # for ftp/sftp at a specific web host

    anki-bin
    keepassxc
    libreoffice
    simplescreenrecorder

  ];
}
