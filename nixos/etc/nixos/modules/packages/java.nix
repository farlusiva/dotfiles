# java.nix
#
# Things for java development.

{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    jdk
    plantuml
    scenebuilder
    openjfx19
  ];
}
