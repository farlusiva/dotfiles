# network.nix
#
# Packages related to networking

{ pkgs, ... }:

{

  # Don't let NetworkManager be able to stall booting
  systemd.services.NetworkManager-wait-online.enable = false;

  environment.systemPackages = with pkgs; [

    # For nm-applet.sh, which I want to be able to use without an internet
    # connection.
    stalonetray
    networkmanagerapplet

    rclone

  ];
}
