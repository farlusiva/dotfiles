# game.nix
#
# Packages related to games

{ pkgs, ... }:

{

  programs.steam.enable = true;

  environment.systemPackages = with pkgs; [
    snes9x-gtk
  ];
}
