# common.nix
#
# Common packages


{ pkgs, ... }:

let
  # Run mk_nix_channels.sh
  unstable = import <unstable> {};
in

{
  environment.systemPackages = with pkgs; [

    tmux htop
    ranger neofetch
    openssh mutt
    w3m # for mutt
    unstable.cmus
    newsboat
    btop # a cooler htop
    fastfetch

    vdirsyncer khal

    # Larger programs

    imagemagick
    ffmpeg-full

  ];

  # Disable SSH askpass because it's annoying.
  programs.ssh.enableAskPassword = false;
  programs.ssh.askPassword = "";
}
