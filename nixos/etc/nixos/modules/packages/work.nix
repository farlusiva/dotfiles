# work.nix
#
# Packages related to work

{ pkgs, ... }:

let
  unstable = import <unstable> { config.allowUnfree = true; };
in

{
  environment.systemPackages = with pkgs; [

    openconnect
    networkmanager-openconnect

    #remmina
    # https://github.com/NixOS/nixpkgs/issues/203976
    #(remmina.override { freerdp = (freerdp.override { openssl = pkgs.openssl_1_1; }); })
    #freerdp # for remmina

    #unstable.zoom-us
    slack

  ];

  # See https://github.com/NixOS/nixpkgs/issues/203976
  #nixpkgs.config.permittedInsecurePackages = [
  #  "openssl-1.1.1v"
  #];

}
