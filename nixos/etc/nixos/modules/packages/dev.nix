# dev.nix
#
# Things for development


{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    gcc
    hyperfine
    ghc
  ];
}
