# vim.nix
#
# Installs and sets up Vim, with a plugin manager.
#

{ config, lib, pkgs, vimPlugins, ... }:

let

  my_vim = with pkgs; ((vim_configurable.override {
    pythonSupport = true; # Required by UltiSnip (python 3)
    guiSupport = (if (config.services.xserver.enable) then "gtk3" else "false");
    ftNixSupport = false;
  }).overrideAttrs (ps: rec {

    # Integrate vim-plug into the vim install instead of using
    # /etc/vimrc, which feels more hacky.
    # The global vimrc sources /etc/vimrc, so vim-plug will in any case
    # still be sourced even when vim is called with -u NONE.
    #
    # I could have installed vim-plug with vim_configurable.customize, but it
    # has multiple problems. It creates a new vim package without the other vim
    # binaries (gvim, vimdiff), and is a shell script calling vim -U,
    # overriding the default vimrc in ${vim_configurable}/share/vimrc.

    nativeBuildInputs = [ findutils ] ++ ps.nativeBuildInputs;
    postInstall = ''
      dir="$(find $out/share/vim -maxdepth 2 -type d -name autoload)"
      cat << EOF > $dir/plug.vim
      source ${pkgs.vimPlugins.vim-plug}/plug.vim
      EOF
      ${ps.postInstall}
      '';
  }));

in

{
  environment.systemPackages = [
    my_vim
    pkgs.vimPlugins.vim-plug
  ];

  # Make vim the default editor.
  environment.variables = {
    EDITOR = "vim";
    VISUAL = "vim";
  };
}
