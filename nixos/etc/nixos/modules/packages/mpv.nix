# mpv.nix

{ pkgs, ... }:

let

  # Run mk_nix_channels.sh
  unstable = import <unstable> {};

  # I could've used overrideAttrs to build mpv newer than 0.33, which will
  # include https://github.com/mpv-player/mpv/pull/9209, which will make mpv
  # prefer yt-dlp over youtube-dl.
  #
  # It turned out that this didn't work for me (even though I had the git
  # version of mpv and had removed youtube-dl from my system), so I instead
  # made mpv prefer yt-dlp in mpv.conf.

  my_mpv = with pkgs; (mpv.override {

    # mpris is used for playerctl control.
    #
    # TODO: Configure sponsorblock to skip over more sections than just
    # 'sponsor'? See https://github.com/po5/mpv_sponsorblock/blob/master/sponsorblock.lua.
    scripts = with mpvScripts; [ sponsorblock mpris ];

  });

in
{
  environment.systemPackages = [
    my_mpv
    unstable.yt-dlp
  ];

}
