# python.nix
#
# This module has an 'extra stuff' option, which can be set from the file
# importing this one. This was done as a learning exercise and is not really
# needed.
#
# Taken from https://nixos.wiki/wiki/Extend_NixOS#Generic_Module_Configuration
#

{ config, pkgs, lib, ... }:

let
  cfg = config.services.pythonInstallation;
in

{

  options = {
    services.pythonInstallation = {
      # Taken to be enabled by default as I can't really see why someone would
      # explicitly import this without wanting it, except perhaps if to be able
      # to conditionally disable it.
      enable = lib.mkOption {
        default = true;
        type = lib.types.bool;
        description = "Install python with this module.";
      };
      pyExtra = lib.mkOption {
        default = false;
        type = lib.types.bool;
        description = "Install extra things.";
      };
    };
  };


  config = lib.mkIf cfg.enable { # ???

    environment.systemPackages = with pkgs; [

      # run jupyter ("notebook") with "jupyter notebook"
      (python3.withPackages (ps: with ps; [
        tkinter
        numpy
        matplotlib
        pillow
        scipy
        autograd
        notebook
        gmpy2
      ] ++ (if (cfg.pyExtra == true) then [ # Extra python packages
        pudb # graphical pdb
    ] else [ ] )
      ))

    ] ++ (if (cfg.pyExtra == true) then [ # Extra packages
      pypy3 # for performance, seldom used
    ] else [ ] );
  };

}
