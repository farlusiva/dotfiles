# modules/laptop.nix
#
# This adds things common for laptops

{ pkgs, ... }:

{
  # Enable the 'light' program, with the things.
  # Requires the user to be in the 'video' group.
  programs.light.enable = true;

  # Enable tlp
  services.tlp.enable = true;

  # Enable suspending
  powerManagement.enable = true;

  environment.systemPackages = with pkgs; [
    acpi # See the battery level
  ];
}
