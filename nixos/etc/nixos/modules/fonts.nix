# fonts.nix
#
# This installs (and sets up) fonts.

{ pkgs, ... }:

{
  # Install tamzen and make it accessible to programs.
  environment.systemPackages = with pkgs; [ tamzen ];
  fonts.packages = with pkgs; [ tamzen ];
}
