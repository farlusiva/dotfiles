# tablet.nix
#
# For tablets or tablet-like functionailty, like a touch screen.
#
# Currently only used by hestia.

{ lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    xournalpp
    binutils # xournalpp
    gnome3.adwaita-icon-theme # xournalpp

  ];

  services.xserver.wacom.enable = true;
}
