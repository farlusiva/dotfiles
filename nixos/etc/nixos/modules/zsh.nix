# zsh.nix
# not in this config file: setting the user's shell to zsh
#

{ lib, pkgs, ... }:

{
  # Install powerlevel10k. There's no need to install zsh, as it's handled by
  # setting programs.zsh.enable to true.
  environment.systemPackages = with pkgs; [
    zsh-powerlevel10k
    any-nix-shell
  ];

  # Enable zsh (so we can set promptInit), and set promptInit.
  programs.zsh.enable = true;
  programs.zsh.promptInit = ''
  source ${pkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme
  # make nix-shell use zsh when in zsh
  any-nix-shell zsh | source /dev/stdin
  '';


  # From hereon, we remove unwanted things from the global zsh config.
  # One can see if this works by inspecting the global zshrc.

  # Remove the pesky default aliases
  environment.shellAliases = {
    l = null;
    ls = null;
    ll = null;
  };

  # It's set in the zshrc, so there's no need for it more than once.
  programs.zsh.enableGlobalCompInit = false;

  # If not forced, then the "command-not-found"-thing
  # overrides it, which I don't want
  programs.zsh.interactiveShellInit = lib.mkForce "";

  # Here are some other things, that should be empty by default.
  environment.interactiveShellInit = "";
  environment.shellInit = "";
  programs.zsh.shellAliases = { };
  programs.zsh.shellInit = "";
}
