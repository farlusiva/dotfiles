# hidpi.nix
#
# HiDPI config for hestia
#
# TODO: Fix HiDPI for applications such as qutebrowser (Qt-related?)

{ pkgs, ... }:

{
#  services.xserver.dpi = 166;
  #hardware.video.hidpi.enable = true; # No longer any effect asof nixos 23.05

  # Set some of the options that the former hardware.video.hidpi set

  # overrides one thing in efi.nix
  boot.loader.systemd-boot.consoleMode = "1";
#  fonts.fontconfig.dpi = 166;
}
