# modules/proprietary.nix
#
# Here are the nonfree programs, or programs whose associated server-side
# programs aren't fully free.

{ pkgs, ... }:

let
  # Remove that pesky ' - Preview'-thing.
  teams = pkgs.teams.overrideAttrs (ps: rec {
    installPhase = ''
      substituteInPlace "share/applications/teams.desktop" \
        --replace 'Microsoft Teams - Preview' 'Microsoft Teams'
      ${ps.installPhase}
    '';
  });

  unstable = import <unstable> { config.allowUnfree = true; };
in

{
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    #teams
  ];
}
