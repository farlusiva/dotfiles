# bluetooth.nix
#

{ lib, pkgs, ... }:

{

  hardware.bluetooth = {
    enable = true;
    powerOnBoot = false;
  };

#  hardware.bluetooth.settings = {
#      # BT headset battery power in upower(1)
#      General.Experimental = "330859bc-7506-492d-9370-9a6f0614037f";
#    };
#  services.upower.enable = true;

}
