# modules/nix.nix
#
# This module sets my settings for nix itself.

{
  # Save some space
  nix = {
    settings.auto-optimise-store = true;
    gc.automatic = true;
    gc.dates = "weekly";
    gc.options = "--delete-older-than 14d";
    settings.sandbox = true;
  };
}
