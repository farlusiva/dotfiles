{ config, vars, lib, ... }:
# vim: nowrap

# This is a work in progress for the moment.
#
# TODO: Make idempot_build_dotfiles.sh, a) work, and b) a part of the iso.
#       Make the greeter inform that the script requires network connectivity.
#
# TODO: Alternatively make a nix package containing the dotfiles and have a
#       script (in PATH) to stow them on startup.
# TODO: A greeter that says the passwords for the users (root, john).
# TODO: Make booting better (make it not look like a bona fide NixOS installer)
# TODO: Install networking like nmtui
# TODO: Maybe remove some heavier applications like latex, gimp,
#       firefox-devedition? (would be done by moving them to heavy.nix, or by
#       making two heavy.nix-es?)
# TODO: See hestia.nix, dionysus.nix and apollo.nix for what else should be
#       added.
# TODO: Make it not autologin to the user 'nixos'?

# Documentation:
# https://nixos.org/manual/nixos/stable/index.html#sec-building-cd
# https://nixos.wiki/wiki/Creating_a_NixOS_live_CD

{

  imports = [
#    <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-base.nix>
#    <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>

    # Fra https://nixos.wiki/wiki/Creating_a_NixOS_live_CD
    <nixpkgs/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix>
    # Provide an initial copy of the NixOS channel so that the user
    # doesn't need to run "nix-channel --update" first.
    <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>

#    ./iso.nix
    ./modules/X.nix
    ./modules/console.nix
    ./modules/efi.nix
    ./modules/fonts.nix
    ./modules/i18n.nix
    ./modules/laptop.nix
    ./modules/nix.nix
    ./modules/packages/anonymity.nix
    ./modules/packages/common.nix
    ./modules/packages/dev.nix
    ./modules/packages/graphical.nix
    ./modules/packages/latex.nix
    ./modules/packages/utils.nix
    ./modules/packages/vim.nix
    ./modules/printing.nix
    ./modules/proprietary.nix
    ./modules/zsh.nix
    ./users/john.nix

  ];

  # isoImage.splashImage = lib.mkForce /etc/nixos/assets/grub_big.png;
  isoImage.volumeID = lib.mkForce "rice-live";
  isoImage.isoName = lib.mkForce "rice-live.iso";

  users.users.john.password = "";
  users.users.root.password = "";

  services.xserver.videoDrivers = [ "nvidia" "amdgpu" "vesa" "modesetting" ];

  # TODO: Give this a higher priority than that in console.nix.
#  environment.etc.issue = lib.mkForce {
#    text = ''
#
#      NixOS ${config.system.nixos.release} (${config.system.nixos.codeName}) on kernel \r (\l)
#
#      The users 'john' and 'root' both have empty passwords.
#
#    '';
#    target = "issue";
#  };
}
