#!/usr/bin/env bash
#
# To be ran by systemd.userActivationScripts. Its job: To set up the dotfiles
# for an initial graphical install
#
# https://old.reddit.com/r/NixOS/comments/g8c734/how_to_clone_a_repository_at_a_specific_location/
#
# NOTE: This file is mostly laying around from an old attempt to idempotently
# set up the dotfiles (as the current stow-based setup is deliberately still
# nix-independent for compatibility with other distros), so I could make a
# bootable iso with this setup. See iso.nix for the rest of it.


GIT=git
USER=john
REPO="https://gitlab.com/farlusiva/dotfiles"

NAME="$(basename "$0")"

if [ -d /home/$USER/dotfiles ] || [ -f /home/$USER/dotfiles ]; then
	echo "$NAME: It was already set up (it seems); aborting."
	exit 0
fi

# TODO: If this script is ran as root, make it run itself as 'USER', so we can
# avoid all this runuser stuff.

runuser -l "$USER" -c "$GIT clone '$REPO' '/home/$USER/dotfiles'" \
runuser -l "$USER" -c "cd '/home/$USER/dotfiles'" \
runuser -l "$USER" -c "cd '/home/$USER/dotfiles' && stow bin bspwm cmus dunst feh neofetch picom python qutebrowser ranger scripts tmux urxvt vim X zathura zsh"

cd /home/$USER/dotfiles
stow -t / nixos
