# configuration.nix
#
# Config for apollo, a laptop server.
#
# To start an installation on wifi with a non-ansi keyboard layout:
#
#    # systemctl start sshd
#    # loadkeys no
#    # wpa_supplicant -B -i (interface) -c <(wpa_passphrase '(SSID)' '(Key)')
#    $ ip route get 8.8.8.8 | awk '(FNR == 1) {print $7}' # (get the local IP)
#    $ passwd nixos
#    (ssh in from a computer on the network)
#    (continue installing by partitioning)


{ config, lib, pkgs, ... }:

{
  imports = [
    <nixpkgs/nixos/modules/installer/scan/not-detected.nix>
    ../modules/efi.nix
    ../modules/i18n.nix
    ../modules/laptop.nix
    ../modules/packages/utils.nix
  ];


  networking.hostName = "apollo";
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Enable networkmanager
  networking.networkmanager.enable = true;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp8s0.useDHCP = true;
  networking.interfaces.wlo1.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Don't suspend when the lid closes
  services.logind = {
    lidSwitch = "ignore";
    lidSwitchDocked = "ignore";
    lidSwitchExternalPower = "ignore";
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    tmux htop
  ];

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    permitRootLogin = "no";
    passwordAuthentication = false;
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.apollo = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" ]; # Enable ‘sudo’ for the user.
    password = "apollo";
    createHome = true;

    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDR5J59lC8n8QpPgxCMsFr0YaJmXAMdE4IvEyzA4ulnUUE3y9aIB7a5ktrGA1SbeUTnkh47V+5mQ9yexZwlsA8RXXmqasXNxYfY+r5S3TQR+wnXXi03zU5dF6J6xF3QGp5QQW16KLrzCOxnmSomoiDzofQSA2i7wMX2alcjQhJyxVTdu6QfUK4t+mxDXMJMdh9+CitM54NSvFGvHMF1Ht7ZloMsP2aC4irsu6YAgUZ7D/TftHx+A+aBf73Xho5niV+HlioC3JZuL1ihxITOhSj23RFmdBbV0yvxE3bgif7aCKT+8YEGz/TI0j0JBeDz4g4xUxuICf0WPLvVEwxApU+uwsL9pKEH5ihhRb7wC4xoXKREmYSzABXGqfSYsDOyK7RsHbxhBqvYkYD71NZHsJ8c4X2O/9eLFXJ81218PJ00uQbME+QpHiVtKmAYwDAV4OsciY7q9JSnCppATTxcpEw/q2q9YHmPIDazXWRdZjsBlQcbu2dxKmieiOWEeRBuPCeTW0UXO85GrQme6aVbBgOSwi+LkGpYdvtwLar0PvRIuXKtl5GGAnE7vy+2aJS1J6+MVpfQXFROZBrO4pPw7YaCheE8/5/314dEhDwnDUpr4Mf4WQy8ZfrsWdaLsVmX1WDnVduv9OOTK/vtsFc+AW4PGN2U3rJKeql8+y/T4RnybQ== athena"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDAGCeqjOtHvNV76f/+Y+mcaU/yAjKJfWKApYifQeFQqOeEC6zVVG6jmtzDlFWt6oRyxgIXmWxPw695Lf1WjqhyYMvsu0nE1HG3sUkepklUdvLD6/nwHKI7l+XTuyJFvCvEOLT8NFcHg+l+Wb1F+wTgcJz7Cji6tZwOw8w2tv35hGIm3URmBDT6J/2AHzWBrMrV5JC8V2yW8QSF5O4oanHeTDtV6XOJ3wz4CPOljTQ4z8k/iL6t3YwayYTC7FWu9Khv0vclr1UwU1UR1yqf5XYbuVTdePrW6Emk0Tt2wy4s0JezjsH6LgZRNlkKaZaN3w0fzWsmrUBKv25hnTObMt1rNoOgvKNlldw+hVFxXnoKvTO79lOkGqD5JbSf64c+WS3xAzxyNVXOsHlMeRJyEdpso28MNVFbrVImwcjsrGrbSAG7tiUvw+4+t5qfjyQxZVpv8veV4J/z9o/YK15ylK8aHWOqZbnvYqZ8eIHefgsV9a3BwWB5WCXUyaR4pAjIkDiYpFAGcAPaFqwFQN/Khd4WWolWtMaTovUDBdT4hniluQXbSGBOY6de3o9mlCfUHZXaiDefplCz+wZj7LoCy+fMjqbCK2iILvRPe5DYdr4X2F/KG/9TP8I26oTskuRhXGtXWPFzlzHcQF0zFo6/OxBhysK/+KTL7Ut+Xwt4vdWb4w== dionysus(nixos)"
    ];

  };

  # Remember how to login from SSH.
  # TODO: Maybe don't make the password public to all with physical access?
  environment.etc.issue = lib.mkForce {
    text = ''

    NixOS ${config.system.nixos.release} (${config.system.nixos.codeName}) on kernel \r (\l)

    Find your local IP with "ip route get 8.8.8.8 | awk '(FNR == 1) {print $7}'"
    or log in as apollo/apollo.

    '';
    target = "issue";
  };


  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?


  ### hardware-configuration.nix


  boot.initrd.availableKernelModules = [ "ehci_pci" "ahci" "usb_storage" "sd_mod" "rtsx_pci_sdmmc" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/030b5504-be8e-4f63-86a4-bfc9c951d053";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/CB78-0C81";
      fsType = "vfat";
    };

  swapDevices = [ ];

  nix.maxJobs = lib.mkDefault 2;
  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
}
