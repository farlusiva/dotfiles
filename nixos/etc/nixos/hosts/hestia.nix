# hestia.nix
#
# This is the configuration.nix for hestia. To use it, symlink this file into
# dotfiles/nixos/etc/nixos as configuration.nix, and then run stow -t / nixos
# as normal.
#
# Warning! HiDPI. The DPI is 166.

{ config, lib, pkgs, ... }:

{
  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

  imports = [
    <nixpkgs/nixos/modules/installer/scan/not-detected.nix>
    ../modules/X.nix
    ../modules/bluetooth.nix
    ../modules/console.nix
    ../modules/efi.nix
    ../modules/fonts.nix
    ../modules/hidpi.nix
    ../modules/i18n.nix
    ../modules/laptop.nix
    ../modules/nix.nix
    ../modules/packages/anonymity.nix
    ../modules/packages/common.nix
    ../modules/packages/dev.nix
    ../modules/packages/graphical.nix
    ../modules/packages/java.nix
    ../modules/packages/latex.nix
    ../modules/packages/mpv.nix
    ../modules/packages/networking.nix
    ../modules/packages/python.nix
    ../modules/packages/scimath.nix
    ../modules/packages/theme.nix
    ../modules/packages/utils.nix
    ../modules/packages/vim.nix
    ../modules/packages/work.nix
    ../modules/printing.nix
    ../modules/proprietary.nix
    ../modules/sysctl.nix
    ../modules/tablet.nix
    ../modules/zsh.nix
    ../users/john.nix
  ];

  # Install the extra python features from python.nix
  services.pythonInstallation.pyExtra = true;

  networking.hostName = "hestia"; # Define your hostname.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.wlp4s0.useDHCP = true;
  networking.networkmanager.enable = true;

  # Fix scrolling
  services.libinput.touchpad.naturalScrolling = true;

  # Fix the scrolling that the option above "breaks":
  environment.etc."X11/xinit/xinitrc.d/10-fix-scrolling.sh" = {
    text = ''
    #!/bin/sh
    xinput set-prop "ETPS/2 Elantech TrackPoint" "libinput Natural Scrolling Enabled" 0
    '';
    mode = "0555";
  };

  # Disable the touchpad as the BIOS doesn't do it when told to.
  environment.etc."X11/xinit/xinitrc.d/10-disable-touchpad.sh" = {
    text = ''
    #!/bin/sh
    xinput set-prop "ETPS/2 Elantech Touchpad" "Device Enabled" 0
    '';
    mode = "0555";
  };

  # Turn the keyboard backlight off, which for some reason gets turned on,
  # sometimes.
  environment.etc."X11/xinit/xinitrc.d/10-fix-backlight.sh" = {
    text = ''
      #!/bin/sh
      light -s sysfs/leds/tpacpi::kbd_backlight -S 0
      '';
    mode = "0555";
  };

  # Set up sshd, but do not set it to start by default.
  # To start: 'systemctl start sshd'.
  services.openssh = {
    enable = true;
    settings = {
      PermitRootLogin = "no";
      PasswordAuthentication = false;
    };
  };
  # Ensure sshd doesn't auto startup
  systemd.services.sshd.wantedBy = lib.mkForce [ ];

  ##############################################################################
  # Below are things that would otherwise be in the hardware-configuration.nix #
  ##############################################################################
  boot.initrd.availableKernelModules = [ "xhci_pci" "nvme" "usb_storage" "sd_mod" "rtsx_pci_sdmmc" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/001be43d-91c3-468f-a487-47da42b99977";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/2133-491F";
      fsType = "vfat";
    };

  swapDevices = [ ];

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
}
