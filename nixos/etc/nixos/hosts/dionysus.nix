# dionysus.nix
#
# This is the configuration.nix for dionysus. To use it, symlink this file into
# dotfiles/nixos/etc/nixos as configuration.nix, and then run stow -t / nixos
# as normal.


{ config, lib, pkgs, ... }:

{
  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?


  imports = [
    <nixpkgs/nixos/modules/installer/scan/not-detected.nix>
    ../modules/X.nix
    ../modules/bluetooth.nix
    ../modules/console.nix
    ../modules/efi.nix
    ../modules/fonts.nix
    ../modules/i18n.nix
    ../modules/laptop.nix
    ../modules/nix.nix
    ../modules/packages/anonymity.nix
    ../modules/packages/common.nix
    ../modules/packages/dev.nix
    ../modules/packages/graphical.nix
    ../modules/packages/latex.nix
    ../modules/packages/mpv.nix
    ../modules/packages/networking.nix
    ../modules/packages/python.nix
    ../modules/packages/scimath.nix
    ../modules/packages/theme.nix
    ../modules/packages/utils.nix
    ../modules/packages/vim.nix
    ../modules/packages/work.nix
    ../modules/printing.nix
    ../modules/proprietary.nix
    ../modules/zsh.nix
    ../users/john.nix
  ];

  # Networking

  # Define the hostname.
  networking.hostName = "dionysus";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s25.useDHCP = true;
  networking.interfaces.wlp3s0.useDHCP = true;
  networking.interfaces.wwp0s29u1u4i6.useDHCP = true;

  networking.networkmanager.enable = true;
  # Set passwords in nmtui(1)


  ##############################################################################
  # Below are things that would otherwise be in the hardware-configuration.nix #
  ##############################################################################


  # Also set tp_smapi (from https://github.com/NixOS/nixos-hardware).
  boot.initrd.availableKernelModules = [ "ehci_pci" "ahci" "usb_storage" "sd_mod" "sdhci_pci" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" "tp_smapi" ];
  boot.extraModulePackages = with config.boot.kernelPackages; [ tp_smapi ];

  hardware = {
    cpu.intel.updateMicrocode = true;
  };

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/9adb6180-7615-4d45-9de5-bfd61066b9bf";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/E33B-2F9A";
      fsType = "vfat";
    };

  swapDevices = [ ];

  nix.maxJobs = lib.mkDefault 4;
  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
}
