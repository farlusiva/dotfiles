# .zshrc
#
# There is some clutter here and there, which is mostly from oh-my-zsh before I
# purged it (but kept the good parts).


################################################################################
#                                   aliases
################################################################################

# Aliases
alias feh="feh -Z."
alias grep="grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}"
alias history="fc -l 1"
alias ls="ls --color=auto"
alias pacman="sudo pacman"
alias trizen="trizen --nocolors"
alias -g ...="../.."
alias -g ....="../../.."
alias -g .....="../../../.."
alias -g ......="../../../../.."
alias e="$VISUAL"
cd() { echo "No."; } # Enforce the use of 'k'. This is temporary.

# More ergonomic cd
# New: Enfore use of the ranger cd-aliases.
k() {
	case $1 in
		dotfiles)  echo "No; use gk!" ;;
		Dropthing) echo "No; use gj!" ;;
		Downloads) echo "No; use gd!" ;;
		*)
			builtin cd $@
		;;
	esac
}

# Haskell/ghc doesn't like tabs, but I do.
alias runghc="runghc -Wno-tabs"
alias ghc="ghc -Wno-tabs"

# Functions, i.e. "complicated aliases"
fetch() {
	clear
	#neofetch $@
	fastfetch $@
}

status() {
	command -v syncthingctl >/dev/null || return 1
	while sleep 0.5; do
		syncthingctl -s --stats | grep -v "^\s*Version"
	done
}

kal() { # Interactive calendar
	vdirsyncer sync && \
	ikhal && \
	vdirsyncer sync
}

kall() { # Alternative to cal(1)
	khal calendar today 14d
}

nixrun() {
	if [ "$#" -eq 1 ]; then
		nix-shell -p "$1" --command "$1"
	elif [ "$#" -eq 2 ]; then
		nix-shell -p "$2" --command "$1"
	fi
}

update() {
	if grep -q NixOS /etc/os-release; then
		sudo -v
		nice nix-channel --update
		sudo nice nixos-rebuild switch --upgrade-all
		sudo -k
	elif grep -q Arch /etc/os-release; then
		if [ "$1" = "--fix-keyring" ]; then
			echo "$0: Fixing keyring"
			pacman -Sy archlinux-keyring
		else
			nice trizen --nocolors -Syu
		fi
	else
		echo "Wrong OS."
		return 1
	fi
}

# Get the local ip. Slightly hacky.
localip() {
	ip address | awk 'BEGIN {prt=1;} /^[0-9]+: / {prt=1;} /^[0-9]+: lo: / {prt=0;} // {if (prt==1) {print;}}' \
	           | awk '/\s*inet 192\.168.0\./ {gsub("/24", "", $2); print $2}'
}

# A command to quickly enter my vim environment for specifically writing
# equations to be copied and used elsewhere.
tv() {
	# Special vim options: This
	# - makes the systemwide clipboard the default
	# - rebinds yanking a single line (yy, Y) to not include whitespace and EOL
	# If the user wants to yank an entire line, they'll have to use "y_".
	cat << EOF | vim - -c 'set ft=tex' -c 'set clipboard^=unnamedplus' \
	-c 'nnoremap yy <Cmd>let cp = getpos(".")<CR><Cmd>norm ^yg_<CR><Cmd>call setpos(".", cp)<CR>' \
	-c 'nmap Y yy' -c 'call feedkeys("2jA")' --not-a-term
\begin{equation*}
\begin{split}
	
\end{split}
\end{equation*}
EOF
}

# Git aliases
alias gitclean="{ git reset; git checkout .; git clean -fdx }" # Clean git repo
alias gc="git commit"
alias gdh="git diff HEAD"
alias gds="git diff --staged"
alias gl="git log --date=iso"
alias gs="git status"
alias gsh="git show HEAD"

# Ranger cd-aliases
# ('builtin cd' is required because 'cd' is still aliased to a "No.")
#alias gh="builtin cd" # not needed, 'k' does it better
alias gm="builtin cd /var/run/media" # collision with graphicsmagick, an octave dependency
alias gM="builtin cd /mnt/hdd"
alias gj="builtin cd ~/Dropthing"
alias gd="builtin cd ~/Downloads"
alias gk="builtin cd ~/dotfiles"

# For fzf (normal use w/o pipe) and for other programs that try to use it
FD_COMMAND="fd --hidden -I -E .git -E .cache -E .stversions" # For own use
export FZF_DEFAULT_COMMAND="$FD_COMMAND"
export FZF_DEFAULT_OPTS="--exact --bind=ctrl-z:ignore --tiebreak=length"


# Play music (sound files with album art, or without video) with a visualiser
# https://trac.ffmpeg.org/wiki/FancyFilteringExamples
ffmusplay() {

	if ffprobe $1 2>&1 | grep -qi video | grep -qE "jpeg|png|jpg" \
	                   || ffprobe $1 2>&1 | grep -qvi video && \
	                   [ ! -z "$@" ]; then

		# Escape the commas and apostrophes.
		filename="$(echo $1 | sed -e 's/,/\\,/g' -e "s/'/\\\\\\\\\\\\'/g")"
		env ffplay -f lavfi "amovie=$filename,asplit=2[out1][a]; \
		                     [a]showspectrum=color=channel:scale=sqrt:\
		                     orientation=vertical:overlap=1:s=2048x1024:\
		                     slide=scroll[out0]"
	else
		env ffplay $@
	fi
}

mpvaudio () {
	mpv -vo=null "$@"
}


################################################################################
#                                   options
################################################################################


## Options
export VISUAL=vim # The one true editor
export EDITOR="$VISUAL"
bindkey -e # Prefer emacs binds for the shell, though
setopt interactivecomments
setopt auto_cd # cd to folders by typing their names (mostly used for '..' &c.)
setopt multios
setopt prompt_subst # Required by agnoster
export LESS="MRicj3" # Better git diffing
WORDCHARS='*?_-.[]~=&;!#$%^(){}<>' # Only kill words on <C-w>, not WORDS

# Python
export PYTHONSTARTUP="$HOME/.pythonrc"

################################################################################
#                                   history
################################################################################

HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt append_history
setopt extended_history
setopt hist_ignore_dups # ignore duplication command history list
setopt hist_ignore_space
setopt hist_verify
setopt inc_append_history
setopt share_history

# Fuzzy history searching. The terminfo stuff is taken from oh-my-zsh.
if [[ "${terminfo[kcuu1]}" != "" ]] && [[ "${terminfo[kcud1]}" != "" ]]; then
	autoload -U up-line-or-beginning-search
	autoload -U down-line-or-beginning-search
	zle -N up-line-or-beginning-search
	zle -N down-line-or-beginning-search
	bindkey "${terminfo[kcuu1]}" up-line-or-beginning-search
	bindkey "${terminfo[kcud1]}" down-line-or-beginning-search
fi


################################################################################
#                                  completion
################################################################################

zstyle ":completion:*" menu select # Menu-driven completion
# Complete case-insensitively if no case-sensitive match is found
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'

# Initialise completion
# TODO: Find a faster way to do this. Removing it shaves off ~20ms.
autoload -U compaudit compinit
compinit -i -d "$HOME/.zcompdump-${HOST/.*/}-$ZSH_VERSION"
# Colours on tab-completion of files/directories
autoload -U colors && colors
eval "$(dircolors -b)"
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"

################################################################################
#                                   keybinds
################################################################################


bindkey \^U backward-kill-line # Make <C-u> do what it should do
# Enable and bind the "edit the current command"-function.
autoload -U edit-command-line
zle -N edit-command-line
bindkey "^x^e" edit-command-line

################################################################################
#                                   various
################################################################################

# From oh-my-zsh, this puts the terminal in application mode when zle is active.
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
  function zle-line-init()   { echoti smkx }
  function zle-line-finish() { echoti rmkx }
  zle -N zle-line-init
  zle -N zle-line-finish
fi

# Run neofetch as that nice welcome screen, but only on non-ssh connections
# where the fetching is not explicitly disabled.
env | grep -q "^SSH_CLIENT\|^NO_FETCH\|IN_NIX_SHELL\|^PROJECTEDIT_SHELL" || fetch


################################################################################
#                                powerlevel10k
################################################################################


# Make the nix-shell prompt look like the 'context' prompt.
POWERLEVEL9K_NIX_SHELL_BACKGROUND=black
POWERLEVEL9K_NIX_SHELL_FOREGROUND=gray

# If in nix-shell, then use that instead of the 'context' prompt.
if [[ -n $IN_NIX_SHELL ]]; then
	POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(background_jobs status nix_shell dir vcs)
else
	POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(background_jobs status context dir vcs)
fi

# Make powerlevel10k mimic agnoster.
POWERLEVEL9K_DISABLE_RPROMPT=true
POWERLEVEL9K_CONTEXT_TEMPLATE="%F{gray}%n@%m"
POWERLEVEL9K_VCS_GIT_HOOKS=(vcs-detect-changes git-untracked)
POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND='yellow'
POWERLEVEL9K_VCS_UNTRACKED_ICON="\b" # Compensates for a space before the icon?
POWERLEVEL9K_VCS_STAGED_BACKGROUND='black'
POWERLEVEL9K_STATUS_OK=false
POWERLEVEL9K_STATUS_CROSS=true

# Change some icons for the prompt to look better.
# CHeck internal/icons.zsh in powerlevek10k for more
POWERLEVEL9K_VCS_UNSTAGED_ICON="#"
POWERLEVEL9K_VCS_STAGED_ICON="+"

# Copied from a config generated by 'p10k configure'. It makes the
# prompt shorten dir names by only showing the shortest unique prefix for all
# folders, except for directories containing any of the files given in
# anchor_files, such as git repos.
typeset -g POWERLEVEL9K_SHORTEN_STRATEGY=truncate_to_unique
local anchor_files=(
	.bzr .citc .git .hg .node-version .python-version .go-version .ruby-version
	.lua-version .java-version .perl-version .php-version .tool-version
	.shorten_folder_marker .svn .terraform CVS Cargo.toml composer.json go.mod
	package.json stack.yaml
)


# NixOS handles loading it by itself. See nixos/.../zsh.nix.
if ! grep -q NixOS /etc/os-release; then
	source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
fi


################################################################################
#                                   manpages
################################################################################

# For the nice manpages
man() {

	export LESS_TERMCAP_mb=$(printf $'\e[1m\e[38;5;251m') # tput bold; tput setaf 251
	export LESS_TERMCAP_md=$(printf $'\e[1m\e[38;5;251m') # tput bold; tput setaf 251
	export LESS_TERMCAP_me=$(printf $'\e[0m') # tput sgr0
	export LESS_TERMCAP_so=$(printf $'\e[1m\e[38;5;255m') # tput bold; tput setaf 255
	export LESS_TERMCAP_se=$(printf $'\e[27m\e[0m') # tput rmso; tput sgr0
	export LESS_TERMCAP_us=$(printf $'\e[4m\e[38;5;251m') # tput smul; tput setaf 251
	export LESS_TERMCAP_ue=$(printf $'\e[24m\e[0m') # tput rmul; tput sgr0
	export LESS_TERMCAP_mr=$(printf $'\e[7m') # tput rev
	export LESS_TERMCAP_mh=$(printf $'\e[2m') # tput dim
	export LESS_TERMCAP_ZN="" # tput ssubm
	export LESS_TERMCAP_ZV="" # tput rsubm
	export LESS_TERMCAP_ZO="" # tput ssupm
	export LESS_TERMCAP_ZW="" # tput rsupm

	env man "$@"
}

################################################################################
#                                    ranger
################################################################################

# ranger_cd for the shell
ranger_cd() {
    temp_file="$(mktemp -t "ranger_cd.XXXXXXXXXX")"
    env ranger --choosedir="$temp_file" "$@"
    if chosen_dir="$(cat -- "$temp_file")" && [ -n "$chosen_dir" ] && [ "$chosen_dir" != "$PWD" ]; then
        builtin cd -- "$chosen_dir"
    fi
    rm -f -- "$temp_file"
}

# https://thevaluable.dev/zsh-line-editor-configuration-mouseless/
_ranger_cd() {
	CUTBUFFER=""
	zle .kill-whole-line
	zle -U " ranger_cd$CUTBUFFER"
}

# This binds Ctrl-O to ranger-cd:
zle -N _ranger_cd
bindkey "^O" _ranger_cd
ranger() { echo "No."; } # This is temporary and is meant to make <C-o> a habit.

################################################################################
#                               fzf in the shell
################################################################################

# Will find the first folder in the input (meant
# to be the pwd) that contains a .git directory.
_get_git_dir() {
	INP="$1"
	while :; do

		if [[ -d "$INP/.git" ]]; then
			echo "$INP"
			return 0
		fi

		if [[ "$INP" = "/" ]] || [[ -z $INP ]]; then
			return 1
		fi

		INP="$(dirname "$INP")"
	done
}


# Edit a file or cd into its dir.
# Press ctrl-e => cd to dir and edit the file
# Press ctrl-t => edit the file (no cd)
# Press ctrl-k => cd to file (no edit)
# Press ctrl-r => open in ranger (ranger_cd, see above)
# Press enter: do same action as arg (ctrl-e, ctrl-t, ctrl-k)
# Note: $= makes zsh do word splitting: https://unix.stackexchange.com/a/461361


# File input methods ("MODE"):
# 1: The files you select from are found in your working directory
# 2: The files you select from are from a preselected list of dirs, such as a
#    list of favourite dirs. Usually used for more "global" cd-ing.

# Usage examples:
# _fzf-edit ctrl-e 1
# _fzf-edit ctrl-k 2 -f fav_dirs.txt project_dirs.txt
# _fzf-edit ctrl-o 2 -d ~/Syncthing ~/dotfiles ~/programming_project

# Note: MODE=2 has not been tested for weird unicode-named files. MODE=1 has.

_fzf-edit() {
	if [[ -z "$1" ]]; then
		echo "$0: need to know default behaviour (ctrl-{e,k,t}); exiting."
		return 1
	fi
	DEF_PRESS="$1"
	if [[ -z "$2" ]]; then
		echo "$0: need to know file input methods (1 or 2)"
		return 1
	fi
	MODE="$2"
	shift
	shift
	FOLDER=()
	if [[ $MODE = "2" ]]; then
		# Motivating example: FOLDER=("~/Syncthing" "~/dotfiles")
		if [[ -z "$1" ]]; then
			echo "$0 needs dir(/s) (-d/-D) file(/s) to read dirs from (-f/-F)"
			return 1
		fi

		# -d: take in one dir, -f: read in dirs from one file.
		# -D: Like -d, but take all furthers args as -d's. Correspondingly -F.
		while [[ $# -ne 0 ]]; do
		case "$1" in
		-d) shift; FOLDER+=("$1"); shift ;;
		-D) shift; FOLDER+=("$@"); break ;;
		-f) shift; while read -r line; do FOLDER+=("$line"); done < $1; shift ;;
		-F) shift; while read -r line; do FOLDER+=("$line"); done < $@; break ;;
		*) echo "$0: Failed to parse args."; return 1 ;;
		esac
	done
	fi
	# Folder to search in.
	# If the _get_git_dir line is uncommented, search from the root of the git
	# repo the user is in, if one exists. (Else from current dir).
	if [[ $MODE = "1" ]]; then
		# We allow the "relative" stuff for mode 1, but not mode 2,
		# which has its own logic.
		FOLDER=("$(_get_git_dir "$(pwd)" || echo ".")")
		FOLDER=("$(realpath --relative-to=. "$FOLDER")")

	elif [[ $MODE = "2" ]]; then
		# Perform similar string processing as in the case above.

		for ((k = 1; k <= $#FOLDER; k++)); do
			# Perform string substitution to allow for literal ~ in the input
			TMP="$FOLDER[k]"
			TMP=${TMP//'~'/$HOME} # hack
			FOLDER[k]="$(realpath -z "$TMP")"
		done
	fi
	# TODO: Find a way to shorten the lengths of the output files in MODE=2.

	# give fd extra args to show paths as relative, when in a git repo and not
	# in its root.
	# Performance hit: Noticeable on nixpkgs (~30k lines). Therefore we only
	# do this when there are <5k items.
	FD_GIT_REL=""
	if [[ "$FOLDER" != "." ]] && [[ "$MODE" = 1 ]]; then

		# for 5k, this takes ~12 ms, says hyperfine, on hestia (NVME).
		tmp="$($=FD_COMMAND -t f -t l . $FOLDER --max-results 5000 | wc -l)"
		if [[ "$tmp" != "5000" ]]; then
			FD_GIT_REL="--batch-size 1000 -X realpath --relative-to=. -zsm"
		fi
	fi

	setopt localoptions pipefail no_aliases 2> /dev/null
	local sel="$($=FD_COMMAND -t f -t l -0 . $FOLDER $=FD_GIT_REL | fzf +m --reverse \
	          --height 10 --expect=ctrl-t,ctrl-e,ctrl-k,ctrl-r,ctrl-o \
	          --read0 $=FZF_DEFAULT_OPTS --bind=ctrl-j:accept)"
	local press="$(echo "$sel" | head -n 1)"
	sel="$(echo "$sel" | tail -n +2)"
	if [[ -z "$sel" ]]; then
		zle redisplay
		return 0
	fi
	# $press is "" if the 'normal' selection is done
	if [[ -z "$press" ]]; then
		press="${DEF_PRESS}"
	fi
	zle push-line # Clear buffer. Auto-restored on next prompt.
	local dir="$(dirname "$sel")"

	case "$press" in
	ctrl-e)
		sel="$(realpath --relative-to="$dir" "$sel")"
		[[ "$dir" != "." ]] && BUFFER=" builtin cd ${(q)dir} && "
		BUFFER+="$EDITOR ${(q)sel}"
		;;
	ctrl-r|ctrl-o) BUFFER=" ranger_cd --selectfile=${(q)sel}" ;;
	ctrl-t) BUFFER+=" $EDITOR ${(q)sel}" ;;
	ctrl-k) [[ "$dir" != "." ]] && BUFFER+=" builtin cd ${(q)dir}" ;;
	esac
	zle accept-line
	local ret=$?
	unset sel press dir # ensure this doesn't end up appearing in prompt expansion
	zle reset-prompt
	return $ret
}

# Decide between the emacs <C-e> or fzf-edit when <C-e> is pressed.
# I've decided not to move fzf-edit to another bind (for now? <C-t> can work).
# It activates fzf-edit when the current line is empty.
_ctrl-e() {
	if [[ -z "$BUFFER" ]]; then
		_fzf-edit ctrl-e 1
	else
		zle end-of-line
	fi
}

zle -N _ctrl-e
bindkey "^E" _ctrl-e

_ctrl-k() {
	if [[ -z "$BUFFER" ]]; then
		_fzf-edit ctrl-k 1
	else
		zle kill-line
	fi
}
zle -N _ctrl-k
bindkey "^K" _ctrl-k

# Bind <C-j> on empty line to fzf-edit "global movement" mode.
_ctrl-j() {
	if [[ -z "$BUFFER" ]]; then
		_fzf-edit ctrl-k 2 -d ~/Dropthing -d ~/dotfiles -f ~/.fzfedit_files.txt
		#_fzf-edit ctrl-k 2 -f ~/list.txt
	else
		zle accept-line
	fi
}
zle -N _ctrl-j
bindkey "^J" _ctrl-j


# fzf-based file completion in zsh
# We use '. "$1"' instead of just a "$1" in order to complete paths that are
# partially written, such as when typing /usr/share/**<TAB>.

_fzf_compgen_path() {
	fd -H . "$1"
}

_fzf_compgen_dir() {
	fd -H -t f . "$1"
}

# Hack to get it to work on NixOS.
# The fzf path could be passed onto us here with an environment variable set in
# zsh.nix, but I reckon that this is better because it doesn't rely on zsh.nix.
source "$(dirname "$(realpath "$(which fzf)")")/../share/fzf/completion.zsh"


################################################################################
#                               aliases, part 2
################################################################################


# To be used to aliasing a function to have some default arguments, but only
# when the user hasn't given any themselves. This is done to avoid misleading
# the user. For instance, if 'sage' is aliased to 'env sage -q', then the user
# typing 'sage --help' will not work as expected, because the shell tries to
# run 'sage -q --help', making sage give an error.
#
# Usage:
# 	sage() { _def_cli_arg sage -q "$@"; }
# to run sage -q when 'sage' is given no args form the user, but not otherwise.
_def_cli_arg() {
	cmd="$1"
	arg="$2"
	shift 2
	if [[ -z "$@" ]]; then
		env $cmd "$arg"
	else
		env $cmd "$@"
	fi
}

# Less intrusive way of making sage,python,octave not display their welcome
# messages.
python() { _def_cli_arg python -q "$@" }
sage() { _def_cli_arg sage -q "$@" }
octave() { _def_cli_arg octave -q "$@" }

################################################################################
#                                   colour
################################################################################

# Try to make fzf look nice with very little colour.
# This also looks nice when pywal is on.
export FZF_DEFAULT_OPTS="
    $FZF_DEFAULT_OPTS --no-color
	--color bg:-1,bg+:-1,fg+:-1,fg+:regular,hl+:8,hl:8,hl:regular,hl+:regular
"

# Pastel wants this.
# TODO: Is there a better way to handle this?
if [ "$TERM" = "st-256color" ] || [ "$TERM" = "rxvt-unicode-256color" ]; then
	export COLORTERM="truecolor"
fi
