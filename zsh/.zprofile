#
# ~/.zprofile
#

# Set the XDG variables to avoid polluting ~.
# See https://wiki.archlinux.org/title/XDG_Base_Directory.
# Note: bspwm and sxhkd need XDG_CONFIG_HOME.
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

export EDITOR="vim"
export VISUAL="vim"


# Add ~/.bin to the PATH if it exists, and already isn't there.
# [:^] and [:$] don't work for the regex.
if [[ -d ~/.bin ]] && [[ ! $PATH =~ "(^|:)$HOME/.bin($|:)" ]]; then
	PATH+=":$HOME/.bin"
fi


# Only start X on tty1, go to the shell (implicitly) else.
if [ -z "$DISPLAY" ] && [ "$XDG_VTNR" -eq 1 ]; then

	exec startx

elif [ -z "$DISPLAY" ] && [ "$XDG_VTNR" -eq 3 ]; then
	# In case zsh or its config fails or is undesirable, such as OOM situations

	exec sh

else

	# We don't need to run our zshrc when we're just going into X11.
	# Because of the 'exec', once X11 is killed, the tty will just
	#  jump back into the login screen.
	# This also reduces time from the login screen to X.
	[ -f ~/.zshrc ] && source ~/.zshrc
fi
