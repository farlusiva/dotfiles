#!/bin/false
# vim: syn=sh
#
# wm independent hotkeys
#
#
# inherits the following environment variables (and assume they're set):
# PREF_TERM, ALT_TERM, DMENU

# brightness
XF86MonBrightness{Up,Down}
	~/.config/bspwm/light.py {up,down}

# sound
# fallback to the shift-bind (playerctl) if cmus isn't running
XF86Audio{Play,Stop,Prev,Next}
	P="playerctl"; echo | cmus-remote 2>/dev/null && cmus-remote {-u || $P play-pause, \
	-s || $P pause && $P position 0, -r || $P previous, -n || $P next}
	# Bad solution above. If sxhkd had a nice feature, it'd be possible to do
	# something along the lines of this instead:
	# echo | cmus-remote && cmus-remote -{u,s,r,n} || \
	# playerctl -i cmus {play-pause,pause;playerctl -i cmus position 0,previous,next}

alt + {Down,Up,Left,Right}
	P="playerctl"; echo | cmus-remote 2>/dev/null && cmus-remote {-u || $P play-pause, \
	-s || $P pause && $P position 0, -r || $P previous, -n || $P next}

# seek +- 5 seconds
# (fallback to playerctl cmus isn't running)
ctrl + XF86Audio{Prev,Next}
	var="{-,+}"; echo | cmus-remote 2>/dev/null && cmus-remote -k "$var"5 || \
	playerctl -i cmus position "5$var"
alt + ctrl + {Left,Right}
	var="{-,+}"; echo | cmus-remote 2>/dev/null && cmus-remote -k "$var"5 || \
	playerctl -i cmus position "5$var"

# Same as above, but now with playerctl ignoring cmus.
# Also give "play" and "pause" the same behaviour as cmus-remote.
shift + XF86Audio{Play,Stop,Prev,Next}
	playerctl -i cmus {play-pause,pause;playerctl -i cmus position 0,previous,next}
shift + alt + {Down,Up,Left,Right}
	playerctl -i cmus {play-pause,pause;playerctl -i cmus position 0,previous,next}
shift + ctrl + XF86Audio{Prev,Next}
	playerctl -i cmus position 5{-,+}
shift + alt + ctrl + {Left,Right}
	playerctl -i cmus position 5{-,+}

# Basic audio functionality
XF86Audio{RaiseVolume,LowerVolume,Mute}
	~/.config/bspwm/volume_script.sh running \
	set-sink-{volume XXX +5%,volume XXX -5%,mute XXX toggle}

# Set the volume to 50%
alt + XF86Audio{Raise,Lower}Volume
	~/.config/bspwm/volume_script.sh running set-sink-volume XXX 50%


# Mute all and unmute all
alt + {_,shift} + XF86AudioMute
	var="{true,false}"; \
	~/.config/bspwm/volume_script.sh all set-sink-mute XXX "$var"; \
	if [ "$var" = "true" ]; then msg="muted all"; else; msg="unmuted all"; fi; \
	notify-send -a volume_script -u low "$msg"


# launch some programs
# in order: terminal, file manager, lock, unicode program,
# dotfile-editor, make sxhkd reload its config, program launcher
alt + {Return,r,z,Escape,d,m}
	{"$PREF_TERM", "$PREF_TERM" -e ranger_shell.sh,  \
	~/.config/bspwm/unicode_glue.sh, pkill -USR1 -x sxhkd && notify-send \
	"reloading sxhkd", j4-dmenu-desktop --no-generic --term="$PREF_TERM" \
	--dmenu="$DMENU",file_opener.sh}

# It's more complicated than those above, so separate it
alt + x
	slock &; sleep 0.1; xset q | grep -q "DPMS is Enabled" && xset dpms force standby

# launch some programs, but with shift held down
# In order: alternative terminal, poweroff
alt + shift + {Return, x}:
	{"$ALT_TERM" -e sh, dmenu_sysutils.sh --dmenu "$DMENU"}

# scratchpad
alt + c
	id="$(xdotool search --class scratchpad)"; \
	workspace="$(xprop -root _NET_CURRENT_DESKTOP | busybox awk '\{print $3 +1\}')"; \
	if [ "$id" != "" ]; then \
		bspc node "$id" --flag hidden -f -d "^$workspace"; \
		bspc desktop -f "^$workspace"; \
	fi


# clear the clipboard
alt + n
	for k in clipboard buffer-cut primary secondary; do \
		printf '' | xclip -in -selection "$k"; \
	done

# create a receptacle, an "empty" frame; or kill all of them in the workspace.
alt + {_,shift} + a
	{bspc node -i,\
	bspc query -N -d -n '.leaf.!window' | while read p; do bspc node "$p" -k; done}


# Take a screenshot
Print
	flameshot gui

# enable/disable the bar in a way that refreshing the bspwmrc won't change
# When focusing multiple windows, undo alt+u, i.e. rotate the other way.
# (-s won't do anything if the daemon became disabled)
alt + shift + u
	if bspc query -N -d -n '.focused.!window' >/dev/null; then \
		bspc node -R 270; \
	else \
		LEMOND=~/.config/bspwm/lemond.sh; $LEMOND -T; $LEMOND -s; \
	fi

#
# bspwm hotkeys
#

# restart bspwm
alt + shift + r
	bspc wm -r

# close and kill
alt + {_,shift} + q
	bspc node -{c,k}


# send the newest marked node to the newest preselected node
alt + y
	bspc node newest.marked -n newest.!automatic.local


#
# state/flags
#

# set the window state
alt + {t,shift + t,s,f}
	bspc node -t {tiled,pseudo_tiled,floating,fullscreen}

# set the node flags
alt + ctrl + {m,x,y,z}
	flag="{marked,locked,sticky,private}"; \
	bspc node -g "$flag"; \
	bspc wm -g | sed -e "s/^$(bspc config status_prefix)//" -e \
	             's/:\([mM]\)/\n\1/' | sed -e "/^[^M]/d" -e  "s/^.*:G//" | \
	             grep -qi "$(echo "$flag" | cut -c-1)" && state="on"; \
	notify-send -a sxhkd_flags "flag: $flag; now $\{state:-off\}"

#
# focus/swap
#

# focus the node in the given direction
# If there is no window/leaf to that direction, focus/move the window to the
# desktop to that direction instead.
alt + {_,shift} + {h,j,k,l}
	arg="{f,s}";\
	dir="{west,south,north,east}";\
	if [ -z "$(bspc query -N -n "$dir.window.local")" ]; then;\
		if [ "$arg" = "f" ]; then;\
			bspc query -M -m "$dir" >/dev/null && bspc monitor -f "$dir";\
		else \
			bspc node -m "$dir" --follow;\
		fi;\
	else \
		bspc node -"$arg" "$dir";\
	fi

# select the parent
alt + p
	bspc node -f @parent

# focus the next/previous node in the current desktop
alt + {_,shift} + e
	bspc node -f {next,prev}.local.!hidden.window

# move to the desktop right/left of the current one, or move the window there.
# Note that the braces in the awk call are escaped to make sxhkd not touch them.
# This "line" is close to a hardcoded 512 character limit in sxhkd. See
# https://github.com/baskerville/sxhkd/issues/139 for details.
# Note: If I need to shorten this more (e.g. for functionality), then remove
# SEL as a variable and the [ -n "$SEL" ] thing; they aren't strictly needed.
# The same goes for the "continue" in the ($k ~ /^[M]/) block.
alt + {i,o} + {_,shift}
	dir={1,-1}; \
	SEL="$( \
	bspc wm -g | sed -e "s/^$(bspc config status_prefix)//" -e \
	's/:\([mM]\)/\n\1/' | sed -ne '/^M/p' | busybox awk -F: -v dir="$dir" '\{ \
		for (k = 1; k <= NF; k++) \{ \
			if ($k ~ /^[M]/) \{ \
				mon=$1; \
				gsub(/^[Mm]/, "", mon); \
				continue; \
			\} \
			if ($k ~ /^[MmLTG]/) continue; \
			if ($k ~ /^[FOU]/) \{ \
				dsktp = k; \
				break; \
			\} \
		\} \
		to_sel=$(dsktp+dir); \
		if (to_sel ~ /[MmLTG]/) exit; \
		gsub("^[fouFOU]", "", to_sel); \
		print(mon ":^" to_sel); \
	\}')"; \
	[ -n "$SEL" ] && bspc {desktop -f, node -f -d} "$SEL"

# Similar to alt+{i,o}, but when ctrl is held, do the same with monitors.
alt + ctrl + {_,shift} + {i,o}
	arg="{no_shft,shft}";\
	dir="{east,west}";\
	if [ "$arg" = "no_shft" ]; then \
		bspc monitor -f "$dir"; \
	else \
		bspc node -m "$dir"; \
	fi

# focus or send to the given desktop on the current monitor
alt + {_,shift} + {1-9,0}
	bspc {desktop -f,node -d} focused:'^{1-9,10}'

#
# preselect
#

# preselect the direction
alt + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

# preselect the ratio
alt + ctrl + {1-9}
	bspc node -o 0.{1-9}

# cancel the preselection(s) for the focused desktop
alt + ctrl + space
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

#
# move/resize
#

# expand a window by moving one of its side outward
super + {h,j,k,l}
	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
super + shift + {h,j,k,l}
	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
alt + shift + {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0}

# rotate some nodes, can also be used for flipping (bspc node -F)
# note: if you ever forget, remember to select the nodes before rotating.
# overridden elsewhere
# alt + {_,shift} + u
# 	bspc node -R {90,270}


# The following binds, due to their specificity, are "available sometimes":
#
# what                         when selecting now many windows?
#
# alt+shift+b                  one
# alt+{_,shift}+t              multiple
# alt+f                        multiple
# alt+s                        multiple
#
#
# Check for how many windows the user is focusing:
# 	bspc query -N -d -n '.focused.!window' >/dev/null


# Balance/unbalance focused nodes so they all occupy the same area
# But, do something else if the user isn't focusing multiple nodes.
alt + {_,shift} + b
	VAR={B,E}; \
	if bspc query -N -d -n '.focused.!window' >/dev/null; then \
		bspc node focused "-$VAR"; \
	else \
		[ "$VAR" = "B" ] && bt-dmenu.sh -d dmenu_wrapper.sh; \
	fi

# Do a lemons thing when focusing one window, and normal rotation when multiple.
# The 0.05 part is because of hestia responsitivity issues.
alt + u
	if bspc query -N -d -n '.focused.!window' >/dev/null; then \
		bspc node -R 90; \
	else \
		if [ -f /tmp/lemons_sngblck ]; then rm /tmp/lemons_sngblck; \
		else touch /tmp/lemons_sngblck >/dev/null; fi; \
		echo "song" >/tmp/lemons; sleep 0.05; echo "song" >/tmp/lemons; \
	fi

#
# binds prefixed by alt + g
#

# alternate between the tiled and monocle layout
alt + g ; m
	bspc desktop -l next

alt + g ; k
	[ -f ~/.config/cmus/lyrics_term.sh ] && ~/.config/cmus/lyrics_term.sh

alt + g ; r
	~/.config/bspwm/rotate_screen.sh --dmenu "$DMENU"

alt + g ; j
	clipboard.sh -d "$DMENU"

alt + g ; n
	bspc rule -a quick_note -o state=floating sticky=on; \
	notes-script.sh -t "st -c quick_note -e" ~/notes/quick.md

alt + g ; p
	theme_setter.sh -d dmenu_wrapper.sh -p ~/dotfiles/wal

# Switch the gap between the normal amount, and nothing.
# The default gapsize is 12; afaik sxhkd can't store it in any reasonable way.
alt + g ; b
	if [ "$(bspc config window_gap)" != "0" ]; then \
		bspc config window_gap 0; \
	elif [ "$(bspc config window_gap)" = "0" ]; then \
		bspc config window_gap 12; \
	fi

# Dunst keybinds; dunst's own config is somewhat unreliable on hestia (but
# not on athena?), so do it here instead.
alt + {period,comma}
	dunstctl {close,history-pop}



# Keybinds to easily start programs on empty workspaces.
# NOTE: These keybinds start with an alt because, after a previous experiment I
# found out that sxhkd will hinder keyrepeat functionality on its binds, even
# if they're ~-ed. See commits march 2022.

# NOTE: When given two 'alt + f'-keybihnds, sxhkd seems to only use the first one,
#       so we use @. An @ might reduce chances of accidentally activating
#       the bind more than once.
@alt + f
	{ bspc query -N -d -n '.focused' || firefox; } >/dev/null

@alt + shift + f
	{ bspc query -N -d -n '.focused' || firefox --private-window; } >/dev/null
