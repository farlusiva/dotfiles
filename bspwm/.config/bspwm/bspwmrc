#!/bin/sh

# Check for a sane environment
for k in xdo xdotool hostname bspc feh xmodmap xset sxhkd st sh killall \
         pgrep kill; do
	if ! command -v "$k" > /dev/null; then
		echo "$k not found; exiting."
		exit 1
	fi
done

# Assume a single-monitor setup
PRIM_MON=""
SEC_MON=""

# hostname-dependent variables
if [ "$(hostname)" = "athena" ]; then
	export FONTSIZE=12
	DUNST_FONT="tamzen 10"
	#PRIM_MON="DP-2:" # athena is multimonitor
	PRIM_MON="HDMI-0:" # athena is multimonitor
	SEC_MON="DVI-I-1:"
elif [ "$(hostname)" = "dionysus" ]; then
	export FONTSIZE=11
	DUNST_FONT="tamzen 10"
elif [ "$(hostname)" = "hestia" ]; then
	export FONTSIZE=14
	DUNST_FONT="tamzen"
else
	export FONTSIZE=20
	DUNST_FONT="tamzen"
fi

export PREF_TERM=st
export ALT_TERM=urxvt
export DMENU=dmenu_wrapper.sh

# Give each monitor their own 10 desktops.
bspc query -M | while read k; do
	bspc monitor "$k" -d 1 2 3 4 5 6 7 8 9 10
done

bspc desktop "${PRIM_MON}^2" -l monocle # Monocle on the 'browser' desktop
bspc config border_width 1
bspc config window_gap 12
bspc config split_ratio 0.5
bspc config borderless_monocle true
bspc config gapless_monocle false
bspc config single_monocle true
bspc config presel_feedback true
bspc config external_rules_command ~/.config/bspwm/external_rules.sh
bspc config ignore_ewmh_focus true # Handle impolite programs
bspc config pointer_follows_monitor true
bspc config remove_unplugged_monitors true

bspc rule -a Zathura state=tiled monitor=focused
bspc rule -a scratchpad state=floating sticky=on hidden=on locked=on monitor=focused layer=above
bspc rule -a qutebrowser desktop="${PRIM_MON}^2" follow=off monitor=focused
bspc rule -a "Anki" state=pseudo_tiled monitor=focused
bspc rule -a "Tor Browser" state=floating monitor=focused
bspc rule -a "steam:*" desktop="${SEC_MON}^9" follow=off monitor=focused
bspc rule -a "Steam:*" desktop="${SEC_MON}^9" follow=off monitor=focused
bspc rule -a "*:Steam" desktop="${SEC_MON}^9" follow=off monitor=focused
bspc rule -a "*:steam" desktop="${SEC_MON}^9" follow=off monitor=focused
bspc rule -a "*:*:Steam" desktop="${SEC_MON}^9" follow=off monitor=focused
bspc rule -a "*:*:steam" desktop="${SEC_MON}^9" follow=off monitor=focused
bspc rule -a "SimpleScreenRecorder" state=pseudo_tiled monitor=focused
bspc rule -a "*:*:Print" state=floating

TMP="^10" # if there's only one (primary) monitor
if [ "$SEC_MON" != "" ]; then TMP="${SEC_MON}^1"; fi
for k in "firefoxdeveloperedition" "firefox-developer-edition" \
         "Firefox Developer Edition" "firefox-aurora"; do
	bspc rule -a "$k" desktop="$TMP" follow=off monitor=focused
done


# Adopt living orphans, if any. Will also happen when bspwm is restarted with
# bspc wm -r.
bspc wm --adopt-orphans

# Sometimes, when weird monitor things happen, bspc may make(?) padding that
# shouldn't be there. Mostly meant for use when bspwm is restarted by a 'wm -r'.
bspc config left_padding 0
bspc config right_padding 0
bspc config top_padding 0 # The bar reserves some, so do this before handling the bar
bspc config bottom_padding 0


# Start some programs:
xset b off &
xmodmap ~/.Xmodmap &

if pgrep sxhkd; then
	pkill -USR1 -x sxhkd
else
	sxhkd
fi &

(
	# The bar
	~/.config/bspwm/lemond.sh -k
	~/.config/bspwm/lemond.sh -s
) &
{ killall picom 2>/dev/null; while ! picom -b; do sleep 0.1; done } &
{ ~/.config/bspwm/volume_script.sh --daemon; } >/dev/null 2>&1 &
pgrep redshift 2>/dev/null || { redshift -x; redshift -Pr; } &
{ pgrep syncthing 2>/dev/null || syncthing; } > /dev/null 2>&1 &
{ pgrep udiskie 2>/dev/null || udiskie -qs; } >/dev/null 2>&1 &
{ pkill dunst; while ! dunst -font "$DUNST_FONT"; do sleep 0.1; done } >/dev/null 2>&1 &

# set the background if unset
{
	if [ -f ~/.cache/wal/wall ]; then
		# Pywal-y integration
		cp ~/.cache/wal/wall /tmp/wall
	elif [ ! -f /tmp/wall ]; then
		convert -size 10:10 "xc:#2d2d2d" /tmp/wall.png
		mv /tmp/wall.png /tmp/wall
	fi
	feh --no-fehbg --bg-fill /tmp/wall;
} &

{ killall unclutter 2>/dev/null; unclutter -b --jitter 20 --ignore-scrolling; } &

# Pywal integration.
# We assume that if pywal is "active" that it was activated by pywal_change.sh.
# If not, set colours and such to be the usual.

if [ -f ~/.cache/wal/colors.sh ]; then

	. ~/.cache/wal/colors.sh
	# Source extra override if any exists
	if [ -f "${HOME}/.cache/wal/bspwm_colours.sh" ]; then
		. "${HOME}/.cache/wal/bspwm_colours.sh"
	fi

	bspc config normal_border_color "$color8"
	bspc config active_border_color "$color2"
	bspc config focused_border_color "$color15"
	bspc config presel_feedback_color "$color4"

	xrdb ~/.cache/wal/colors.Xresources

else # no pywal

	# Colours. Some of these are default, but explicitly stating them here
	# makes for better reloading
	bspc config normal_border_color "#30302f"
	bspc config active_border_color "#474645"
	bspc config focused_border_color "#817f7f"
	bspc config presel_feedback_color "#817f7f"

	xrdb ~/.Xresources # note: also done in the xinitrc

fi

# Must be after loading the pywal xresources for it to be in effect.
{
	while xdotool search --class scratchpad; do
		while xdotool search --class scratchpad windowkill; do
			true
		done
	done
	pgrep -f "$PREF_TERM -c scratchpad" || "$PREF_TERM" -c scratchpad -e sh \
		                                                -c 'tmux attach || tmux'
} &
