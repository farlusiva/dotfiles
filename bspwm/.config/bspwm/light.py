#!/usr/bin/env python3
#
# This script increases the brightness exponentially, giving more control
# when the brightness is low.

from subprocess import check_output
from sys import argv
from math import e, floor, ceil

def shell_output(inp):
	return check_output(inp, shell=True)


# sanity checks
if "not found" in str(shell_output("which light")):
	print("light doesn't seem to be installed.")
	exit(1)

if len(argv) < 2:
	print("needs argument")
	exit(1)


growth = e ** 0.1 # approx 1.105
cur = float(shell_output("light -G"))

if cur <= 0.3:
	growth *= 1.5

# use 1-digit precision instead of 2 (which light gives us) to ensure
#  that light -G gives us exactly what it was given by this script.
if argv[1] == "up":
	new = ceil((cur * growth)*10) / 10
elif argv[1] == "down":
	new = floor((cur / growth)*10) / 10
else:
	print("does not understand argument.")
	exit(1)

if new == 0:
	exit(1)

out = shell_output("light -S " + str(new))
