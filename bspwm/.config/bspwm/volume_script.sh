#!/bin/sh
#
# volume_script.sh
#
# Handles volume-related things.
#
# It takes $1 to decide which kind of sinks to loop over: "running" or "all".
# It takes $@ as argument for sending to pactl, where all instances of the
# string $REPLSTR (currently 'XXX') will be replaced by the sink number.
#
# It takes the option --nonotify as $1. When set, it won't use notify-send when changing
# the volume when bspc reports that the current node  is fullscreened. This will
# be automatically unset if notify-send or bspc are not found.


LEMONS="$(dirname "$0")/lemons.sh" # Path of the lemons.sh that this script
                                 # will source the 'Volume'-function from.
NOTIFY=1 # Whether or not to use the notification functionality by default.
REPLSTR='XXX' # The string to replace the sink number with in the pactl call.

DUNST_ID=72 # hope no one else uses this


# The awk function to use.
_awk() {
	busybox awk "$@"
}


check_deps() {
	for k in "$@"; do
		command -v "$k" >/dev/null || { echo "$k not found; exiting."; exit 1; }
	done
}

usage() {
	echo "usage: $(basename "$0") [--nonotify] (running,any) (args to pactl) " >&2
	echo "       OR" >&2
	echo "       $(basename "$0") --daemon" >&2
}

# Copied from lemons.sh. Note that it gives a line of output per monitor.
_get_ws_data() {
	bspc wm -g | sed -e "s/^$(bspc config status_prefix)//" -e 's/:\([mM]\)/\n\1/'
}

# Are we fullscreen on the focused monitor?
_is_fullscreen() {
	_get_ws_data | grep -q "^M.*T="
}



# Starts a daemon that closes the notifications sent by this program if the
# user exits fullscreen, as then the bar will show the volume, making the
# notification unneccessary.
start_daemon() {

	# Check if there isn't an instance already running
	ps="$(ps aux)"

	if echo "$ps" | grep -v "$$" | grep -q "$(basename "$0") --daemon"; then
		echo "$(basename "$0") $*: Daemon already running; exiting." >&2
		exit 1
	fi

	bspc subscribe node_state | grep --line-buffered "fullscreen off" |
	                            while read -r k; do

		if ! _is_fullscreen; then

			# Not a hack. (Still doesn't remove the entry from the history
			# though; see https://github.com/dunst-project/dunst/issues/308).
			dunstify -C "$DUNST_ID" -a volume_script

			# Hack. Removes the previous notification from this script by
			# replacing it with an empty one.
			#notify-send -a volume_script -u low " " -t 10
		fi
	done

}

main() {

	# The main part
	if [ "$1" = "running" ]; then
		shift
		# Loop over all active sinks
		for k in $(pactl list sinks | grep -C1 '^Sink' | grep -C1 'RUNNING' \
									| _awk -F'#' '/^Sink/ {print $2}'); do
			pactl $(echo "$@" | sed "s/$REPLSTR/$k/g")

		done

	elif [ "$1" = "all" ]; then
		shift
		# Loop over all sinks
		for k in $(pactl list sinks | grep -C1 '^Sink' \
									| _awk -F'#' '/^Sink/ {print $2}'); do
			pactl $(echo "$@" | sed "s/$REPLSTR/$k/g")

		done
	else
		usage
		exit 1
	fi

	# Do not use the notification functionality if notify-send or bspc are not
	# installed.
	if [ "$NOTIFY" -eq 1 ]; then

		for k in notify-send bspc dunstify; do
			if ! command -v "$k" >/dev/null 2>&1; then
				echo "$k not found; turning the notify functionality off." >&2
				NOTIFY=0

				# The rest of the program is about using the notification feature,
				# so we can exit here.
				exit 0
			fi
		done
	fi

	# Are we fullscreen? If so, tell us, as we can't see the bar when something is
	# fullscreened above it.
	if [ "$NOTIFY" -eq 1 ] && _is_fullscreen ; then

		# Source lemons to get access to the volume function.
		# Lemons is a bash script, but the bashisms are in the loop-part of said
		# script, so this script doesn't need to be a bash script because of that.
		if [ -f "$LEMONS" ]; then
			LIBLEMONS="" . "$LEMONS"
		fi

		VOL="$(Volume)"

		# Only make a notification if there is something to display.
		if [ -n "$VOL" ]; then
			#notify-send -a volume_script -u low "$VOL" -t 1000
			dunstify -r "$DUNST_ID" -a volume_script -u low "$VOL" -t 1000
		fi

	fi
}

# Start the daemon if we're doing that
if [ "$1" = "--daemon" ]; then

	check_deps basename ps grep bspc notify-send
	# TODO: Find out whether I actually want this or not. It severely clutters
	# my dunst log, and sometimes misfires. All I get in return is that it
	# removes audio notifications

	# TODO: Find out how dunst can selectively not log notifications from
	# certain providers.

	#check_deps basename ps grep bspc notify-send
	#start_daemon "$@"
	exit

else

	# Various sanity checks.


	# Not enough args.
	if [ "$#" -lt 2 ]; then
		usage
		exit 1
	fi

	check_deps busybox grep pactl basename

	if [ "$1" = "--nonotify" ]; then
		NOTIFY=0
		shift
	else
		check_deps notify-send
		NOTIFY=1
	fi

	main "$@"
fi
