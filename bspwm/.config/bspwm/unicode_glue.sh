#!/usr/bin/env bash
#
# This runs unicode_picker.py and gives the user a dmenu prompt to pick the
# desired character, and then copies it to the three clipboards.

if [ -z "$DMENU" ]; then
	DMENU="$DMENU -l 10"
else
	DMENU="dmenu_wrapper.sh -l 10"
fi

CACHE_FILE="/tmp/unicode_picker"

# Simple caching
if [[ ! -e $CACHE_FILE ]]; then
	~/.config/bspwm/unicode_picker.py -a > $CACHE_FILE
fi

if [[ -f $CACHE_FILE ]]; then
	SEL="$( cat "$CACHE_FILE" | $DMENU )"
else
	SEL="$( ~/.config/bspwm/unicode_picker.py -a | $DMENU )"
fi


if [[ -z $SEL ]]; then
	exit
fi

echo "$SEL" | awk '{printf $1}' | tee >(xclip -in -selection primary) \
              >(xclip -in -selection secondary) >(xclip -in -selection \
              clipboard)


# I'm not sure if this is neccessary, but ensure that dmenu is dead anyways.
killall dmenu

# Select the current window again (iff bspwm is installed)
sleep 0.1
command -v bspc >/dev/null && bspc node -f
