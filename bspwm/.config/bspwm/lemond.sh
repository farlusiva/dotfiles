#!/usr/bin/env bash
#
# A daemon for lemons.sh that can start and stop it. It's supposed to make it
# easier to start and stop the script properly, making for less code in the
# bspwmrc.
#
# This script is as of writing mostly a copy of trayd.sh.
#
# The "disabling" feature is meant to be used to be able to "toggle" the bar,
# and have refreshing bspwm (which calls this with a -s), not start the bar
# again. This could have been implemented client-side by bspwm, but I decided
# to implement it here so that the bspwmrc and sxhkdrc didn't need to
# coordinate for it to work.

# TODO: Killing lemons here doesn't properly tell lemons.sh to
# activate its cleanup before dying. Find out a way to do this,
# so we don't pollute /tmp with lemons tmp stuff


LEMONS=$HOME/.config/bspwm/lemons.sh
FILE="/tmp/lemons_pid"
PIPE="/tmp/lemons"


# to be set by the bspwmrc running this?
# All those who run this script (bspwmrc, sxhkd) are supposed to have the FONTSIZE
# variable set.
if [ -z "$FONTSIZE" ]; then
	FONTSIZE=25
fi

usage() {
	printf "usage: $(basename "$0") [ARG]\n" >&2
	printf "\n" >&2
	printf "Arguments:\n" >&2
	printf "\t-g\tget pid of daemon\n" >&2
	printf "\t-h\tget help\n" >&2
	printf "\t-k\tkill the daemon\n" >&2
	printf "\t-s\tstart the daemon, and fork it\n" >&2
	printf "\t-S\tstart the daemon, and don't\n" >&2
	printf "\t-t\ttoggle -s/-k\n" >&2
	printf "\t-e\tenable the daemon\n" >&2
	printf "\t-d\tdisable the daemon, kill if it exists\n" >&2
	printf "\t-T\ttoggle -e/-d\n" >&2
	printf "\t-i\tget if daemon is enabled\n" >&2
}

# exit code says if a daemon is already running
check_daemon() {

	if is_disabled; then
		return 1
	fi

	if [ ! -e "$FILE" ]; then
		return 1

	elif [ -f "$FILE" ]; then
		return 0
	fi

	echo "$0: error when checking existence of daemon, there's something at $FILE!" >&2
	exit 0
}

kill_other_lemons() {
	# Another instance of lemons.sh is running, so if we try to run it, then
	# it'll error. So kill whatever is already running.
	if [ -e "$PIPE" ]; then
		# Copied from the old bspwmrc setup
		pgrep -f "sh [^ ]*lemons.sh" | while read k; do kill "$k"; done 2>/dev/null
		pkill lemonbar 2>/dev/null
		rm /tmp/lemons 2>/dev/null
	fi
}

kill_daemon() {

	if is_disabled; then
		return
	fi

	if ! check_daemon; then
		return;
	fi

	pid="$(cat "$FILE")"

	echo "Killing $pid"

	# let it handle its own death via the exit trap
	kill "$pid"
	bspwm_cleanup
}

start_daemon() {

	if is_disabled; then
		echo "$(basename "$0"): Daemon is disabled; enable with -e. Exiting."
		return
	fi

	if check_daemon; then
		echo "$(basename "$0") $*: daemon already running (locked by $FILE); exiting." >&2
		exit 1
	fi

	trap cleanup INT TERM EXIT

	kill_other_lemons


	pid="$$"
	echo "$pid" > "$FILE"

	PIPE="$PIPE" $LEMONS \
	| lemonbar -a 20 -u -1 -f "tamzen for Powerline:size=$FONTSIZE" -n lemonbar |
	while read k; do sh -c "$k"; done &

	pid="$!"


	echo "$0: Daemon running. My pid is $pid"

	# Put the bar under the root window, such that fullscrening a window will
	# not show the bar.
	sleep 0.5
	xdo id -a lemonbar | while read k; do
		xdo above -t "$(xdo id -n root)" "$k"
	done

	wait "$pid"

	# cleanup wil be automatically initiated by the exit trap, so nothing
	# to do here.

}

# Safely cleanup
cleanup() {

	cur_pid="$(cat "$FILE" 2>/dev/null)"
	if [[ "$cur_pid" = "$$" ]]; then
		rm "$FILE"
	fi

	# kill all subprocesses
	# https://unix.stackexchange.com/a/706789

	{ pgrep -P $$ | xargs kill; } >/dev/null 2>&1

}

bspwm_cleanup() {
	# When bspwm has reserved some space for the bar (via not ignoring ewmh
	# struts),

	# Note: This must be changed if the bar is put at the bottom.
	TMP="$(bspc config top_padding)"
	bspc config top_padding 0
	bspc config top_padding "$TMP"
}

start_forked_daemon() {
	THIS_FILE="$(realpath "$0")"
	nohup sh -c "$THIS_FILE -S" >/dev/null 2>&1 &
}

is_disabled() {
	if [ -f "$FILE" ]; then
		[ "$(cat "$FILE")" = "-1" ]
	else
		return 1
	fi
}

# disable '-s', '-S' and '-t' by making the pid -1.
# Also kill it if there is any daemon here.
disable_daemon() {
	if is_disabled; then
		return
	fi

	# Kill if exists
	if check_daemon; then
		kill_daemon
	fi
	kill_other_lemons

	echo "-1" > "$FILE"
}

enable_daemon() {
	if is_disabled; then
		rm "$FILE"
	fi
}

while (( $# )); do
	case "$1" in

	-h|--help)
		usage
		exit 0
		;;
	-k)
		kill_daemon
		exit 0
		;;
	-t)
		# flips between -s and -k
		if check_daemon; then
			kill_daemon
		else
			start_forked_daemon
		fi
		exit 0
		;;
	-s)
		echo "starting forked daemon"
		start_forked_daemon
		exit 0
		;;
	-S)
		start_daemon
		exit 0
		;;
	-g)
		if [ -f "$FILE" ]; then
			cat "$FILE" 2>/dev/null
			exit 0
		fi
		exit 1
		;;
	-e)
		enable_daemon
		exit 0
		;;
	-d)
		disable_daemon
		exit 0
		;;
	-T)
		if is_disabled; then
			enable_daemon
			echo "now enabled"
		else
			disable_daemon
			echo "now disabled"
		fi
		exit 0
		;;
	-i)
		if is_disabled; then
			exit 1
		else
			exit 0
		fi
		;;
	-*) # also matches '--*'
		usage
		exit 1
		;;
	*)
		usage
		exit 0
		;;
	esac

done

# if no arg was given
usage
exit 1
