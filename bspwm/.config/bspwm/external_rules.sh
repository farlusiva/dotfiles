#!/bin/sh
#
# bspwm_external_rules.sh
#
# My external rules for bspwm.
#
# Inspiration: gitlab.com/protesilaos/dotfiles/-/blob/v2.2.0/bin/bin/bspwm_external_rules

wid=$1
class=$2
instance=$3

# Copied from the bspwmrc
PRIM_MON=""
if [ "$(hostname)" = "athena" ]; then PRIM_MON="HDMI-0:"; fi

# Get a receptacle. I don't know exactly how this is chosen. I think it's the
# first one found in the tree.
receptacles="$(bspc query -N -n 'any.leaf.!window')"
presel="$(bspc query -N -d focused -n 'any.!automatic.local')"

# Get info from xprop
XPROP_INFO="$(xprop -id "$wid")"
# For our purposes, dialogs are windows that shouldn't be tiled.
echo "$XPROP_INFO" | grep -q _NET_WM_WINDOW_TYPE_DIALOG && IS_DIALOG="yes"
echo "$XPROP_INFO" | grep -q _NET_WM_WINDOW_TYPE_SPLASH && IS_DIALOG="yes"
STAYS_ON_TOP="$(echo "$XPROP_INFO" | grep -s _NET_WM_STATE_STAYS_ON_TOP)"
WM_TAKE_FOCUS="$(echo "$XPROP_INFO" | grep -s WM_TAKE_FOCUS)"

# If there is a presel, spawn on it, no matter which node is focused.
# However, only do this if the window type is *not* a dialog.
if [ -n "$presel" ] && [ -z "$IS_DIALOG" ]; then
	echo "node=$presel"

elif [ -n "$receptacles" ] && [ -z "$presel" ] && [ -z "$IS_DIALOG" ]; then
	# If there is a receptacle, spawn the window on it. Presels take precedence.
	echo "node=$receptacles"
	echo "follow=off"
fi

if [ "$class" = "Anki" ] && [ -n "$IS_DIALOG" ]; then
	echo "state=floating"
fi

# Try to make zoom better to use.
if [ "$instance" = "zoom" ] && [ -n "$STAYS_ON_TOP" ]; then
	echo "state=floating"
fi

if [ "$class" = "Tor Browser" ]; then
	# Only put TB windows in their default desktop if none already exist.
	# Note: When this is supposed to activate, xdotool will report
	# one TB window in existence, so we check for <= 1.

	if [ "$(xdotool search --name "Tor Browser" | wc -l)" -le 1 ]; then
		echo "desktop=${PRIM_MON}^3"
	fi
fi

# Try to handle Steam dialogs
# Not handled yet: Steam windows that are neither dialogs nor the main window.
if [ "$class" = "steam" ] && [ -n "$WM_TAKE_FOCUS" ] && \
   ! echo "$XPROP_INFO" | grep -qsi "Sign in to Steam"; then

	echo "desktop=current monitor=focused state=floating"
fi

# Move dialogs to the focused workspace, so they are seen. This is to
# counteract some rules that move windows of certain types to certain
# workspaces.
if [ -n "$IS_DIALOG" ]; then
	echo "desktop=focused monitor=focused"
fi
