#!/bin/sh
#
# Dmenu prompt to rotate the screen
#
# Also rotates the wacom stuff if enabled
#
# Known bug: When rotating a screen that is not a touchscreen, the wacom
# tablet will still be rotated anyways. In order to prevent this, rotate the
# touchscreen last.

usage() {
	echo "usage: $(basename "$0") [-h|--help] [-d|--dmenu DMENU] [-n|--no-wacom]"
}


DMENU=dmenu

for k in basename awk xrandr; do
	command -v "$k" > /dev/null || { echo "$k not installed"; exit; }
done

if command -v xsetwacom >/dev/null; then
	WACOM=1
fi


while [ "$#" -ne 0 ]; do
	case "$1" in

	-d|--dmenu)
		if [[ -n $2 ]]; then
			echo "$@"
			DMENU="$2"
			shift 2
		else
			echo "$(basename "$0"): Argument needed"
			exit 1
		fi
		;;
	-n|--no-wacom)
		WACOM=0
		shift
		;;
	-h|--help)
		usage
		exit 0
		;;
	esac
done


outputs="$(xrandr 2>&1 | awk '{ if ($2 == "connected") {print $1}}')"


if [ "$(echo "$outputs" | wc -l)" -ge 2 ]; then
	outputs="$(echo "$outputs" | $DMENU -p "Choose output")"
fi

if [ -z "$outputs" ]; then
	echo "$0: No rotation(s) given"
	exit 1
fi


# Could be multiple
echo "$outputs" | while read k; do
	rot="$(printf "normal\ninverted\nleft\nright" | $DMENU -p "Rotate $k:")"

	if [ -z "$rot" ]; then
		continue
	fi

	xrandr --output "$k" --rotate "$rot"

	if [ "$WACOM" -eq 1 ]; then

		case "$rot" in
		  right)  wacom_rot=cw ;;
		  inverted)  wacom_rot=half ;;
		  left)  wacom_rot=ccw ;;
		  normal)  wacom_rot=none ;;
		esac

		xsetwacom --list devices | awk '{gsub(/^.*id: /, ""); print $1}' |
		                           while read -r j; do
			xsetwacom set "$j" Rotate "$wacom_rot"
		done

	fi
done
