#!/usr/bin/env -S busybox sh
# lemons.sh: The lemonbar script
#
# The setup (as it is now) will update portions of the bar in response to
#  'events' which are sent by subprocesses using a named pipe, and regularly
#  update everything as well.
#
# Note: Requires lemonbar-xft-git to support powerline fonts
# Requires acpi, cmus-remote, busybox awk, pactl and coreutils
#
# If the environment variable LIBLEMONS is set, then lemons will exit before
# anything 'lemons'-y is done, such that all functions and variables are
# sourced, but the loops and fifo stuff are not handled.

# Debug log file used when needed. For debugging, run this:
# rm -f /tmp/lemons_log; touch /tmp/lemons_log; tail -f /tmp/lemons_log
DEBUG_LOG="/tmp/lemons_log"

NUM=0
BAIL=0

if [ -f "${HOME}/.cache/wal/colors.sh" ]; then
	# Pywal integration

	. "${HOME}/.cache/wal/colors.sh"

	# Also, follow a lemonbar override if there is one
	if [ -f "${HOME}/.cache/wal/lemons_colours.sh" ]; then
		. "${HOME}/.cache/wal/lemons_colours.sh"
	fi

	# Bail/fallback if any of the variables used here are unset
	if [ -z "$background" ] || [ -z "$foreground" ] || [ -z "$color0" ] \
	   || [ -z "$color1" ] || [ -z "$color2" ]; then

		BAIL=1
	else
		BAIL=2 # success
	fi

	back="%{B$background}" # == color0?
	fore="%{F$foreground}" # == color7?

	af="%{F$color1}"
	bf="%{F$color0}"
	zf="%{F$color2}"

fi

if [ "$NUM" -eq 0 ] && [ ! "$BAIL" -eq 2 ]; then

	# Standard colours. Have been in use for a long time,
	# since at least 2017-10-31. Also, dunst uses these colours.

	# back/foreground where there is no text
	back='%{B#ee202020}' # Same as urxvt/st/$term
	fore='%{F#333333}'

	# foregrounds where there is text
	af='%{F#777777}'
	bf='%{F#999999}'
	zf='%{F#ff0000}' # low on battery
elif [ "$NUM" -eq 1 ] && [ ! "$BAIL" -eq 2 ]; then
	# Experimental, dark scheme. Attempt 1.
	back='%{B#ee202020}'
	fore='%{F#aaaaaa}'
	af='%{F#303030}'
	bf='%{F#202020}'
	zf='%{F#ff0000}'
fi

# TODO: Use standard shell notation here instead? It should be quicker.

# corresponding backgrounds
ab="$(echo "$af" | sed "s/{F/{B/")"
bb="$(echo "$bf" | sed "s/{F/{B/")"
zb="$(echo "$zf" | sed "s/{F/{B/")"

# Focused / unfocused workspaces
yf="$af"
xf="$bf"

# Corresponding backgrounds
yb="$(echo "$yf" | sed "s/{F/{B/")"
xb="$(echo "$xf" | sed "s/{F/{B/")"

# Seperators
RSEP='' # u+e0b2
LSEP='' # u+e0b0
# Which awk to use
_awk() {
	busybox awk "$@"
}


# Check for dependencies
for k in cmus-remote acpi busybox date tr bspc pactl; do
	command -v "$k" > /dev/null || { echo "$k not installed; exiting"; exit 1; }
done


Time() {
	# Prints the time
	TIM="$(date '+%d/%m/%y %I:%M:%S %p')"
	#printf "%s" "%TIM"

	# Enable graphically toggling the tray, without the keyboard.
	printf "%s{A:%s:}%s%s" "%" "trayd.sh -b -t" "$TIM" "%{A}"
}

Battery() {
	# Gets battery information. Provides own module delimiter, so it
	#  can set the modules background to red when the battery is low.

	# Notes:
	#  - Needs $1 to be the modules background.
	#  - Also assumes that only one battery is installed.


	ff="$1"
	bb="$(echo "$1" | tr F B)"

#	echo "No support for device type: " | _awk \
#	echo "Battery 0: Discharging, 9%, 00:59:59 remaining" | _awk \
	acpi -b 2>&1 | _awk \
	                    -v RSEP="$RSEP" \
	                    -v bb="$bb" \
	                    -v ff="$ff" \
	                    -v fore="$fore" \
	                    -v zb="$zb" \
	                    -v zf="$zf" \
	'
	BEGIN {ORS=""} # print literal strings without a newline at the end
	               # this is important as printf sometimes assumes that the
	               # "%"-es for lemonbar, are for itself

	/No support for device type: / {
		print(ff RSEP fore bb " ")
		exit
	}

	# Hack: Pick the first battery, assuming it is the rigt one. A proper
	# solution would be to loop over all batteries present.
	/^Battery 0/ {
		b=int($4)
		if ($0 ~ /Not charging/) { # alt. ($3 ~ /^Not/)?
			a = " POW"
			b = int($5) # special case; the space moves all fields by one
			}
		if ($3 ~ /^Discharging/) a = " BAT"
		if ($3 ~ /^Charging/) a = " CHR"
		if ($3 ~ /^Full/) a = " POW"
		if ($3 ~ /^Unknown/) a = " UNK"

		if (b <= 10 && a != " CHR")
			print(zf RSEP fore zb a " " b "%")
		else
			print(ff RSEP fore bb a " " b "%")
	}

	# the main loop does not run when stdin is *completely* empty, fix it here
	END {
		if (length($0) == 0) {
			print(ff RSEP fore bb " ")
			exit
		}
	}'
}

Volume() {
	# Shows volume of currently running sink(s).

	# Notes:
	#  - The volume shown is the front-left sound
	#  - All relevant information to the script must be shown before the
	#     alsa.name|bluez.alias line, as it's the line where the printing
	#     is performed.
	#  - The "^PA err$" string should never appear in normal pactl output,
	#    so this method, while hacky, should be safe.

	{ pactl list sinks || echo "PA err"; } | _awk '
	BEGIN {tmp = 0; ORS=""}

	/^PA err$/ { print; exit; } # Let the error pass through, if any
	/\s*State/ { STATE = $2 }
	/\s*Mute/ { VOL = $2 }
	(/^\s*Volume/ && VOL == "no") { VOL = $5; gsub(/,$/, "", VOL) }

	(/^\s*alsa\.name|^\s*bluez\.alias|^\s*media\.name/ && STATE == "RUNNING" ) {
		if (VOL == "yes") VOL = "Muted"

		# Prettified alsa.name/bluez.alias is $0 after this:
		gsub(/\s.*alsa\.name = "|\s*bluez\.alias = "|\s*media\.name = "|"$/, "")

		# if not running on the first time: add spacing between the sinks
		if (tmp)
			print(" ")

		print($0 ": " VOL)

		tmp = 1

	}'


}

Song() {
	# Get the current song cmus is playing

	# Allow temporarily disabling for compactness or presentation reasons.
	if [ -f /tmp/lemons_sngblck ]; then return; fi

	# Notes:
	#  - Assumes that the file line is before the title line.

	cmus-remote -Q 2>/dev/null | _awk '

	/status/ { stat=$2 }
	/^file/ { gsub("^.*/", ""); cur=$0 } # Important that the regex is greedy

	/^tag title/ {
		gsub("^tag title ", "")
		if ($0 != "") cur=$0
	}

	END {
		if (stat == "playing")

			# Truncate when needed
			if (length(cur) > 25)
				printf("%.22s...", cur)
			else
				print(cur)


	}'

}

# Format workspace data for Workspaces
# Example output from  'bspc wm -g':
# WMDP-2:O1:o2:o3:f4:f5:f6:f7:f8:f9:f10:LT:TT:G:mHDMI-0:F1:f2:f3:f4:f5:f6:f7:f8:f9:f10:LM
# The 'W' is the output of "bspc config status_prefix".

_get_ws_data() {
#	echo "MDP-2:O1:o2:o3:f4:f5:f6:f7:f8:f9:f10:LT:TT:G:mHDMI-0:F1:f2:f3:f4:f5:f6:f7:f8:f9:f10:LM" \
	bspc wm -g | sed -e "s/^$(bspc config status_prefix)//" -e 's/:\([mM]\)/\n\1/'
#	| _awk -F':' '
#		{for (k = 1; k <= NF; k++) {
#			if (k != 1) printf(":")
#			if (k == 4) gsub(/^./, "u", $k)
#			printf($k)
#		}printf("\n")}'
	# Above: awk can modify the state for debug purposes.
}


Workspaces() {
	_awk -F':' \
	                  -v LSEP="$LSEP" \
	                  -v back="$back" \
	                  -v fore="$fore" \
	                  -v xb="$xb" \
	                  -v xf="$xf" \
	                  -v yb="$yb" \
	                  -v yf="$yf" \
	                  -v zb="$zb" \
	                  -v zf="$zf" \
	'
	BEGIN {ORS=""}
	{
	# Before-separator
	print(xb " " xf)

	# is the monitor for this line, focused?
	mon_is_focused = $1 ~ /^M/ ? 1 : 0

	# get the current monitor
	gsub(/^[Mm]/, "", $1)
	mon=$1

	# Get the layout (see if it is monocle) of the focused desktop
	monocle_focused=0
	for (k = 2; k <= NF; k++) {
		if ($k ~ /^LM/)
			monocle_focused=1

		# T= is set when fullscreen, which (for our purposes) is not monocle.
		if ($k ~ /~T=/) {
			monocle_focused=0
			break
		}
	}


	# Main loop
	for (k = 2; k <= NF; k++) {
		# only run through the k-values that give workspaces to be printed
		if ($k !~ /^[oFOUu]/)
			continue

		focused = 0 # Used for the "EXTRA" thingy so it also works on status==2,
		            # i.e. on urgent workspaces.

		if ($k ~ /^[FO]/ && mon_is_focused) { # focused
			focused = 1
			status = 1
		} else if ($k ~ /^[Uu]/) # urgent
			status = 2
		else if ($k ^ /^[o]/) # unoccupied, unfocused
			status = 3

		gsub(/^[oFOUu]/, "", $k) # the name is now $k

		EXTRA=""
		if (focused == 1 && monocle_focused == 1) {

			# This feels non-posix, but it works on both busybox awk and mawk!
			# Search for "command | getline [var]" in the (g)awk manual.

			"bspc query -N -m -d -n .!hidden.!floating.leaf.window | wc -l" | getline num

			# Some have set single_monocle, so do not show anything if there
			# are <=1 leaves on the desktop.
			if (num >= 2) {
				EXTRA="[" num "]"
			}
		}


		if (status == 2)
			print(zb LSEP fore zb "%{A:bspc desktop -f " mon "\\:^" $k ":} " $k EXTRA " %{A}" zf)
		else if (status == 1)
			print(yb LSEP fore yb "%{A:bspc desktop -f " mon "\\:^" $k ":} " $k EXTRA " %{A}" yf)
		else
			print(xb LSEP fore xb "%{A:bspc desktop -f " mon "\\:^" $k ":} " $k EXTRA " %{A}" xf)
	}

	print(back LSEP fore) # print last separator
	print(fore back)
	print("\n")

}'
}

# To be run before exit. The trap is set up defined in 'main', so that
# the cleanup won't run when exiting due to errors such as those found
# in 'pre_main'.
cleanup() {
	rm "$PIPE" "$TMP"
	exit 0
}

# Checks for a sane working environment
pre_main() {
	# The named pipe. NOTE: This pipe is defined at:
	# lemond.sh, and update_bar.sh (cmus)
	if [ -z "$PIPE" ]; then
		echo "No named pipe defined, or something already has that name." 1>&2
		exit 1
	fi

	# Check if already exists
	if [ -f "$PIPE" ] || [ -e "$PIPE" ] || [ -d "$PIPE" ]; then
		echo "Something at '$PIPE' already exists; exiting."
		exit 1
	fi
}


# Main loop
main() {

	mkfifo "$PIPE"
	TMP="$(mktemp)"

	trap 'cleanup' EXIT INT HUP

	bspc subscribe node_transfer node_add node_remove desktop_focus desktop_layout | while read -r k; do
		echo "workspaces" > "$PIPE"
	done &

	# Update when there are changes to the sinks
	(
		prev_time=0

		pactl subscribe | stdbuf -o0 grep "Event 'change' on sink" | \
		                  while read -r k; do

			# TODO: Replace this with a sleep thread(?) that sends a "allow the
			#       next cur_vol"-message to $PIPE instead of all this timer
			#       stuff. This will make this thread only print cur_time when
			#       something happends, and the main loop decides wheter or not
			#       to act on it. The point is that neither EPOCHREALTIME nor
			#       the %N sequence in date(1) are supported in dash(1), so I
			#       cannot assume that they are posix.

			# Hack. Use EPOCHREALTIME if it exists as %N might be a GNUism.
			if [ -n "$EPOCHREALTIME" ]; then
				cur_time="$EPOCHREALTIME"
			else
				cur_time="$(date +"%s.%N")"
			fi

			# 0.05 s = 50 ms
			if _awk -v cur="$cur_time" -v prev="$prev_time" \
			        'BEGIN {exit (cur-prev > 0.05) ? 0 : 1}'; then
				echo "cur_vol" > "$PIPE"
				# Hack. For some reason the next two lines (in conjunction with
				# the one above) makes this 'work' on hestia, whereas only the
				# first line above works fine on athena.
				sleep 0.01
				echo "cur_vol" > "$PIPE"
				prev_time="$cur_time"
			fi

		done
	) &

	# Sends regular signals to update everything except the workspaces
	while :; do
		echo "regular" > "$PIPE"
		sleep 5
	done &


	# Initialize the workspaces
	wrkspcs="$(_get_ws_data | Workspaces)"

	# First time print. Fixes a visual glitch that can occur, but which is
	# "fixed" after the first update. So this makes the bar look better on
	# startup.
	echo "$back$fore"

	# Note: The 'Battery' function provides its own RSEP+colour delimiter, so it
	#  can change the module background on its own accord.
	# It also needs to be told the bg colour of its module. (here $bf)

	startup=0

	while :; do

		# debug logging; uses a gnuism
		#echo "$(date -Ins): Waiting for inp" >> "$DEBUG_LOG"
		inp="$(cat "$PIPE")" # | head -n 1 to be sure?

		#echo "$(date -Ins): got an inp! $inp" >> "$DEBUG_LOG"

		# Wait for and parse the command
		case $inp in
			cur_vol) cur_vol="$(Volume)" ;;
			song) cur_song="$(Song)" ;;
			workspaces) wrkspcs="$(_get_ws_data | Workspaces)" ;;

			regular)
				cur_time="$(Time)"
				cur_bat="$(Battery "$bf")"

				# These should already be up to date, but ensure that they are
				# periodically updated nevertheless.
				cur_song="$(Song)"
				cur_vol="$(Volume)"
			;;

		esac

		# TODO: Find a better way to make the output of this entire loop, be
		# printed at the same time. Not doing it at the same time will make the
		# bar look wacky. Storing the output in a variable doesn't seem to
		# work, as it is reset upon the next round of the loop.
		#
		# Variables can't be updated inside the loop because it's in a
		# subshell, so we need to create the loop without piping in order for
		# us to be able to do this with a variable. Unfortunately, the
		# solution I found on SE uses a bashism:
		# https://stackoverflow.com/a/4667725
		# TODO: A POSIX one here: https://stackoverflow.com/a/62519122

		printf "\n" > "$TMP"
		i=0
		bspc query -M | while read k; do

			cur_wrk="$(echo "$wrkspcs" | sed -n -e 1p)"
			wrkspcs="$(echo "$wrkspcs" | sed -n -e 1d -e p)"

			if [ "$i" -eq 0 ]; then
				printf "%s" "%{Sf}"
				i=1
			else
				printf "%s" "%{S+}"
			fi

			printf "%s%s%s%s%s%s%s%s%s" \
			       "$back$fore" \
			       "%{l}" \
			       "$cur_wrk" \
			       "%{r}" \
			       "$bf$RSEP$fore$bb $cur_song " \
			       "$af$RSEP$fore$ab $cur_vol " \
			       "$cur_bat " \
			       "$af$RSEP$fore$ab $cur_time " \
			       "$back$fore" \

		done >> "$TMP"

		cat "$TMP"

		# The first cat doesn't seem to be registered by lemonbar or soemthing.
		# So do it twice, but only on the first time.
		if [ "$startup" = 0 ]; then
			cat "$TMP"
			startup=1
		fi

	done
}

# Don't run the loops and such if LIBLEMONS is set.
if [ -z "${LIBLEMONS+x}" ]; then
	pre_main
	main
fi
