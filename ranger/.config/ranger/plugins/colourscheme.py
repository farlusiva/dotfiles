# colourscheme.py
#
# Sets the colourscheme. This is needed as a plugin, because ranger on TTYs
# does not look good with the solarized colourscheme, at least on my machines.
# This is not a problem on a TTY running tmux, but the output of tty(1) is then
# /dev/pts[0-9]*, so we don't have to worry about that.


from __future__ import (absolute_import, division, print_function)
import ranger.api
from ranger.api.commands import Command
import subprocess
import sys
import os

FILE = os.path.expanduser("~/.config/ranger/extra")

class set_colourscheme(Command):
	"""
	:set_colourscheme

	Sets the colourscheme according to my preferences, and respecting
	the override that can be placed in ~/.config/ranger/extra.
	"""
	# TODO: Check how ranger finds its config folder, and use that
	# instead of something hardcoded

	def execute(self):
		# This only applies to linux
		if sys.platform != "linux":
			return


		# First of all, if extra exists, set the colourscheme by it.
		# This is because if we try to do this in rangers normal config
		# and the file doesn't exist, then it complains.
		# If we want more configuration possibilities in the future (and a less
		# safe ranger experience), then just source the file instead.
		try:

			# The 'extra' file contains an override for the colourscheme, should it
			# exist.

			if os.stat(FILE).st_size > 0:
				with open(FILE, "r") as file:
					data = file.read().replace("\n", "")
					self.fm.set_option_from_string("colorscheme", data)

			return

		except (FileNotFoundError, OSError):
			# Doesn't exist? Then continue.
			pass


		# Are we on a tty?
		tmp = subprocess.Popen("tty", shell=True, stdout=subprocess.PIPE,
							   stderr=subprocess.STDOUT)
		if "tty" not in str(tmp.stdout.readlines()[0]):
			# Not in a tty
			self.fm.set_option_from_string("colorscheme", "solarized")
		else:
			# If we're in a tty
			self.fm.set_option_from_string("colorscheme", "default")
