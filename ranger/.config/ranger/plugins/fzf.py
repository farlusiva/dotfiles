# fzf.py
#
# From here: https://www.posvic.eu/2019/01/fzf-integration-in-ranger/
#
# TODO: Specify additional flags given to fzf with arguments from ranger?

from ranger.api.commands import Command

class fzf_select(Command):
	"""
	:fzf_select

	Find a file using fzf.

	With a prefix argument select only directories.

	See: https://github.com/junegunn/fzf
	"""
	def execute(self):
		import subprocess
		import os.path
		fzf = self.fm.execute_command("fd -H . | fzf +m -e",
		                              universal_newlines=True,
		                              stdout=subprocess.PIPE)
		stdout, stderr = fzf.communicate()
		if fzf.returncode != 0:
			return

		fzf_file = os.path.abspath(stdout.rstrip('\n'))
		if os.path.isdir(fzf_file):
			self.fm.cd(fzf_file)
		else:
			self.fm.select_file(fzf_file)

