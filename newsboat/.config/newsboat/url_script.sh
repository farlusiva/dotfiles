#!/usr/bin/env bash
#
# url_script.sh
#
# Parses urls from newsboat and handles them appropriately.


[[ $1 = "" ]] && exit


if [[ $1 =~ youtube.com/watch* ]]; then
	# Spawn and detach mpv
	setsid mpv --force-window=immediate "$1" >/dev/null 2>&1 &
else
	setsid qutebrowser "$1" >/dev/null 2>&1 &
fi
