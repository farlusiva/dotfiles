# vim: syn=dosini

[global]
	### Display ###

	monitor = 0
	follow = keyboard

	geometry = "300x5-30+20"

	# Show how many messages are currently hidden (because of geometry).
	indicate_hidden = yes

	shrink = no
	transparency = 0
	notification_height = 0
	separator_height = 2

	padding = 8
	horizontal_padding = 8

	frame_width = 2

	# Defines color of the frame around the notification window.
	frame_color = "#aaaaaa"
	separator_color = frame

	# Sort messages by urgency.
	sort = yes

	# Don't remove messages, if the user is idle (no mouse or keyboard input)
	# for longer than idle_threshold seconds.
	# Set to 0 to disable.
	# A client can set the 'transient' hint to bypass this. See the rules
	# section for how to disable this if necessary
	idle_threshold = 120

	### Text ###

	# Set the font. (correct size is set by the caller, i.e. the bspwmrc)
	font = tamzen

	# The spacing between lines.  If the height is smaller than the
	# font height, it will get raised to the font height.
	line_height = 0

	icon_position = off
	markup = full
	format = "<b>%s</b>\n%b"
	alignment = left

	# Show age of message if message is older than show_age_threshold
	# seconds.
	# Set to -1 to disable.
	show_age_threshold = 60

	word_wrap = yes
	ellipsize = middle
	ignore_newline = no

	# Stack together notifications with the same content
	stack_duplicates = true

	# Hide the count of stacked notifications with the same content
	hide_duplicate_count = false

	### History ###

	# Should a notification popped up from history be sticky or timeout
	# as if it would normally do.
	sticky_history = yes

	# Maximum amount of notifications kept in history
	history_length = 20

	### Misc/Advanced ###

	# Define the class and title of the windows spawned by dunst
	title = Dunst
	class = Dunst

	startup_notification = false
	verbosity = mesg

	mouse_left_click = close_current
	mouse_middle_click = do_action
	mouse_right_click = close_all

[urgency_low]
	background = "#777777"
	foreground = "#333333"
	timeout = 5

[urgency_normal]
	background = "#999999"
	foreground = "#333333"
	timeout = 5

[urgency_critical]
	background = "#900000"
	foreground = "#ffffff"
	frame_color = "#ff0000"
	timeout = 0

[sxhkd_flags]
	appname = sxhkd_flags
	set_stack_tag = sxhkd_flags
	timeout = 1

[volume_script]
	appname = volume_script
	set_stack_tag = volume
