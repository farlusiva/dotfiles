# Pywal for monotone wallpapers

So you want to use pywal to get cool colourschemes like
[here](https://old.reddit.com/r/unixporn/comments/oc97fb/sway_martian/)
and
[here](https://old.reddit.com/r/unixporn/comments/un7we2/dwm_well_what_do_you_say/)?

Here is a guide on how to make nice colourschemes for monotone backgrounds.
This is a quick and dirty way to make a colourscheme, but note it might not be
a good one.

First of all, pywal only generates 8 colours, and these are only those found in
the image. So if you have a monotone wallpaper, then your rice will be monotone
as well. Colour theory lets us to better by letting us introduce complementary
colours.

Step 1: Install [pastel](https://github.com/sharkdp/pastel).

Use pywal to generate 8 colours from your favourite background,
for instance if you wallpaper is at `/tmp/wall` like mine is, run
`wal -i /tmp/wall`.

You can find the colours themselves in `~/.cache/wal/colors.json`
and `~/.cache/wal/colors`.
For your eight colours, pick some of the eight colours pywal has given you
For instance one black, one white, then 3 from the wallpaper.
Then use pastel to compute the complementary colours. For example,


```
$ pastel complement "#4f3428" "#cc9d80" "#a4553e" | pastel format hex
#28434f
#80afcc
#3e8ca4
```

Now you have your eight colours. We will now create the last eight
by brightening the "base 8", so we get a 16-colour palette:


```
$ pastel lighten 0.1 "#121110" "#4f3428" "#28434f" "#cc9d80" "#80afcc" \
                     "#a4553e" "#3e8da4" "#bda794" | pastel format hex
#2d2b28
#714a39
#396071
#dbb9a4
#a4c6db
#bf6e56
#56a6bf
#d0c1b4
```

And this gives you a 16-colour colourscheme which has some complementary
colours. Note that the ordering of the colours will affect the result.
