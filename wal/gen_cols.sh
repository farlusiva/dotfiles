#!/usr/bin/env bash

# Takes in some colours, makes some complementary colours,
# adds some lighter versions, and generates it in a format suitable
# in a wal config.

# manually fed in.
# Note: The permutation of col1 -- col6 is to be decided by the user.
black="#0f1016"
white="#ddcbc8"
col1="#525182"
col2="#bc97a3"
col3="#715b82"
col4="$(pastel complement "$col1" | pastel format hex)"
col5="$(pastel complement "$col2" | pastel format hex)"
col6="$(pastel complement "$col3" | pastel format hex)"

# The first eight colours are then
# black, col1, col2, col3, col4, col5, col6, white
# and the eight next are these, but lighter

{
echo -ne "$black\n$col1\n$col2\n$col3\n$col4\n$col5\n$col6\n$white\n";
pastel lighten 0.1 "$black" "$col1" "$col2" "$col3" "$col4" "$col5" "$col6" \
                    "$white" | pastel format hex
				} | awk '

	BEGIN { CNT=0; }
	{
		$0 = "\"color"CNT"\": \"" $0 "\",";
		CNT += 1;
		# remove comme a at last line because of json
		if (CNT == 16) { gsub(",", "", $0); }
		print
	}'
