#!/bin/sh
#
# lyrics_term.sh
#
# Spawns a terminal showing the current lyrics, which also is
# Note: This assumes a lot of things, like bspwm, st and the font to use.
# The window is hidden at first so it can be moved without it being visually
# distracting.
#
# Further work: Make it update when the song updates. This would require a
# daemon writing to a file in /tmp and having the pager automatically update
# when it sees something change. It would besides be a change to lyrics.sh, not
# to this script.


FONT="tamzen for Powerline:size=11"

if [ "$(hostname)" = "hestia" ]; then
	FONT="tamzen for Powerline:size=12"
fi

LYRICS_PROGRAM=~/.config/cmus/lyrics.sh
term() {
	setsid st -g 70x24 -f "$FONT" -c lyrics_term -e $@ &
}


for k in bspc xdotool st; do
	if ! command -v "$k" >/dev/null 2>&1; then
		echo "$0: didn't find '$k'; exiting."
		exit 1
	fi
done

# Ensure that there aren't any other lyrics terms, but if there is/are, then
# show it instead so the user can handle it.
if ! xdotool search --class lyrics_term >/dev/null 2>&1; then
	bspc rule -a lyrics_term -o state=floating sticky=on hidden=on
	term "$LYRICS_PROGRAM"

	# Ensure that the terminal can be searched by xdotool by giving it time to
	# initialize
	sleep 0.1
fi

ID="$(xdotool search --class lyrics_term)"
# Same visual gap as the one normal windows have (assuming borders are on).
BSP_GAP="$(echo "$(bspc config window_gap)" "$(bspc config border_width)" | awk '{print $1 + 2*$2}')"

# TODO: Fix the use of 'xdotool getdisplaygeometry', which doesn't work as
# intended on multimonitor setups. (The intended is to get the geometry of the
# currently focused display, which it doesn't).

# Get geometry position for where the window should be.
get_geom_pos() {
	win_geom="$(xdotool getwindowgeometry "$ID" | awk '/Geometry:/ {print $2}' \
	            | sed "s/x/ /")"
	#scrn_geom="$(xdotool getdisplaygeometry)"
	# Xrandr also works on multimonitor setups
	# (Assume screen 0 because that's what I use)
	scrn_geom="$(xrandr  --query | awk '/^Screen 0/ {gsub(/,/, "", $10); print $8" "$10}')"

	# Calculate the position of the top-left corner of the window in pixels.
	# x' = x(screen) - x(window) - bspc_gap, (equiv. with y)
	printf "%s\n%s\n" "$scrn_geom" "$win_geom" | awk -v GAP="$BSP_GAP" '
		BEGIN { x = -GAP; y = -GAP; }
		(NR == 1) {
			x += $1;
			y += $2;
		}
		(NR == 2) {
			x -= $1;
			y -= $2;
		}
		END {print int(x) " " int(y)}
	'
}

xdotool windowmove --sync "$ID" $(get_geom_pos)
bspc node "$ID" -g hidden=off -m focused -l above
bspc node "$ID" -f
