#!/bin/sh
# Opens the 'lyrics' tag of whatever you're playing in cmus with less(1).
# Tries to remove credit listings and other junk, if there is any.
#
# Usage: lyrics.sh [FILE]

for k in cmus-remote ffprobe awk less basename mktemp; do
	command -v "$k" >/dev/null 2>&1 || { echo "$(basename "$0"): $k not installed; exiting"; exit; }
done


_awk() {
	busybox awk "$@"
}

FILE=""

# Get the filename to use.
get_file() {

	if [ -f "$1" ]; then
		echo "$1"
	else
		cmus-remote -Q 2>/dev/null | _awk '
		/^file/ {
			gsub(/^file /, "")
			print
			exit
		}'
	fi

}

# Parse the arg
if [ "$#" -gt 0 ]; then
	FILE="$1"
fi

# Configure less to make 'j' not stop at EOF.
less_conf="$(mktemp "lyrics-less.XXXXXXXXXX" --tmpdir)"
echo "j forw-line-force" > "$less_conf"

cleanup() {
	rm "$less_conf"
}

trap cleanup INT TERM EXIT

## This is the strictly correct solution that works most of the time,
## but not on my opus files for some reason.
#ffprobe -i "$(get_file "$FILE")" \
#	-show_entries format_tags=LYRICS -of default=noprint_wrappers=1 -v error \
#	| _awk '{gsub(/TAG:LYRICS=/, ""); if (/^Credits/ || /^External links/) {exit};
#	        {print} }' | less --lesskey-src="$less_conf"


# Hack. This works by parsing ffprobe's 'header', which does show the lyrics
# on opus files.
ffprobe -i "$(get_file "$FILE")" -print_format default=noprint_wrappers=0 2>&1 | \
	_awk '
			/^\s*(LYRICS|lyrics)/ { prnt = 1; print; next }
			( /^\s*[^: \t]/ && prnt == 1) { exit }
			(prnt) {print}
	' | sed \
		-e "s/^\s*: //" \
		-E -e "s/^\s*(LYRICS|lyrics)(-[-a-zA-Z]+)?\s*: //" | \
	_awk '
		{
		gsub(/TAG:(LYRICS|lyrics)=/, "")
		if (/^Credits/ || /^External links/) {exit}
		print
		}' | LESS="MRicj3" less --lesskey-src="$less_conf"
