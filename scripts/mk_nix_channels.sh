#!/bin/sh
#
# mk_nix_channels.sh
#
# Ensures that there is an unstable channel in case we might need it.

if [ "$(id -u)" -ne 0 ]; then
	echo "$(basename "$0"): This script needs to be run as root."
	exit 1
fi

nix-channel --add https://nixos.org/channels/nixos-unstable unstable
nix-channel --update
