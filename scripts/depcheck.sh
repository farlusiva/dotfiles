#!/bin/sh
#
# depcheck.sh
#
# Checks that dependencies are installed
# NOTE: The dep list per program is not completely accurate.
#        To find which programs have which deps, grep for known deps.

check_deps() {

	for k in "$@"; do
		command -v "$k" >/dev/null || echo "$k is not found."
	done

}


# Programs integral to the setup
check_deps lemonbar bspwm sxhkd zsh picom X j4-dmenu-desktop dmenu

# Utilities or commonly used programs
check_deps stow cmus dunst dunst exiftool exiftool flameshot flameshot lsof \
           mutt neofetch notify-send notify-send qutebrowser ranger \
           simplescreenrecorder udiskie urxvt vim zathura fd fzf \
           stalonetray nm-applet screenkey xcolor \
           syncthing syncthingctl syncthingtray delta mons fastfetch

# syncthingctl is from syncthingtray
# delta is from 'git-delta' on Arch, 'delta' on nixpkgs

# lemons.sh
check_deps cmus-remote acpi busybox date tr xprop bspc killall

# bspwmrc
check_deps xdo xdotool xclip hostname bspc feh xmodmap xset sxhkd st sh \
           convert unclutter pkill kill

# compiling st
check_deps git wget make cc

# qutebrowser
check_deps mpv yt-dlp jshon

# less commonly used things
check_deps pastel playerctl rg

# Latex-related stuff
check_deps latexmk pdflatex biber makeindex

# Check if powerlevel10k is installed

if ! grep -q NixOS /etc/os-release; then
	test -f /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
else
	nix-store -q -R /nix/var/nix/profiles/system | grep -q powerlevel10k
fi || echo "powerlevel10k is not installed."

# Check if tamzen of some sort is installed

if ! grep -q NixOS /etc/os-release; then
	test "$(find /usr/share/fonts -iname "*tamzen*" 2>/dev/null | wc -l)" -gt 1
else
	nix-store -q -R /nix/var/nix/profiles/system | grep -q tamzen-font
fi || echo "tamzen is not installed."


# Recommended programs that are not required (add if needed)
#echo "Checking for recommended programs"
#check_deps


# Do arch-specific tests for the breeze theme.
if [ -f "$(which pacman 2>/dev/null)" ]; then
	pacman -Q breeze breeze-gtk >/dev/null
fi
