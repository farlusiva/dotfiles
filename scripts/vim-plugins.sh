#!/usr/bin/env bash
#
# vim-plugins.sh
#
# A plugin manager for vim. I wrote it such that it doesn't need to be installed
# on the computer for the plugins to work.
# Documentation: The code is the documentation, as I wrote it for own use.
#
# It uses the plugin thing from vim 8.0, so you'll need vim 8.0 or newer.
#
# NOTE: This script is deprecated, as it's not used anymore.
#       It was originally written to install vim-nix, which I needed to edit my
#       NixOS configuration, in a time where I wanted to avoid plugins and
#       plugin managers for the sake of minimalism. I now use vim-plug (see
#       commit on 2021-01-15 for more), so this script isn't needed anymore.


# Plugin directory.
[ -z "$PLDIR" ] && PLDIR="$HOME/.vim/pack"


# The plugins.
# Format: (type) (args...)
PLUGINS=(
	"git https://github.com/LnL7/vim-nix vim-nix" # Redundant on NixOS!

	# For testing and debugging
#	"git https://github.com/tpope/vim-fugitive vim-fugitive"
#	"git https://github.com/tpope/vim-surround vim-surround"
#	"git https://github.com/preservim/nerdtree nerdtree"
)

# Checks for a sane environment.
check_sanity() {

	for k in vim git; do

		if ! command -v "$k" >/dev/null 2>&1; then
			echo "$(basename "$0"): $k not found; exiting."
			exit 1
		fi

	done
}


usage() {
	echo "usage: $(basename "$0") (install|update|uninstall|list)" >&2
}


# Get the directory where a plugin will be.
get_dir() {

	case $1 in

	git)
		name="$3"
		;;
	esac


	echo "$PLDIR/vimpl/start/$name"
}

# Installs a given plugin
install() {
	case $1 in

	git)
		repo="$2"
		DIR="$(get_dir $@)"
		;;
	esac


	if [ -d "$DIR" ]; then
		echo "$(basename "$0"): There's already something at $DIR)" \
			 "aborting installation of plugin '$*'."
		return
	fi

	git clone "$repo" "$DIR"

	# Install documentation if present
	if [ -d "$DIR" ]; then
		vim -u NONE -c "helptags $DIR/doc" -c q
	fi
}


# Uninstalls a repo
uninstall() {
	case $1 in

	git)
		DIR="$(get_dir $@)"
		;;
	esac

	echo rm -rf "$DIR"
	rm -rf "$DIR"
}

# Updates a repo
update() {

	case $1 in

	git)

		DIR="$(get_dir $@)"
		;;
	esac

	(
		if ! cd "$DIR" 2>/dev/null; then
			echo "$(basename "$0"): Couldn't cd into '$DIR';" \
				  "aborting update of this package."
			return
		fi

		echo "$(basename "$0"): updating $DIR"

		git pull
	)
}

main() {

	if [ -z "$1" ]; then
		usage
		exit 1
	fi

	case $1 in

	install)
		# Install the plugins listed in 'PLUGINS'.
		for k in "${PLUGINS[@]}"; do
			install $k
		done
		;;

	update)
		# Update the plugins listed in 'PLUGINS'.
		for k in "${PLUGINS[@]}"; do
			update $k
		done
		;;

	uninstall)
		# Uninstall the plugins listed in 'PLUGINS'.
		for k in "${PLUGINS[@]}"; do
			uninstall $k
		done
		;;

	list)
		# List the plugins
		for k in  "${PLUGINS[@]}"; do
			echo $k
		done
		;;

	*)
		usage
		exit 1
		;;


	esac
}


check_sanity
main $@
