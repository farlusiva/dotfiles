#!/bin/sh
#
# This creates the directory structure for programs, so stow won't link the
# entire path to the dotfiles. For example, if ~/.config does not exist, then
# stowing 'qutebrowser' will make ~/.config a symlink to
# ~/dotfiles/qutebrowser/.config, which is unacceptable.

mkdir -p ~/.config/bspwm
mkdir -p ~/.config/cmus
mkdir -p ~/.config/dunst
mkdir -p ~/.config/dunst/dunstrc.d
mkdir -p ~/.config/fastfetch
mkdir -p ~/.config/feh
mkdir -p ~/.config/gtk-3.0
mkdir -p ~/.config/khal
mkdir -p ~/.config/mpv
mkdir -p ~/.config/mpv/script-opts
mkdir -p ~/.config/neofetch
mkdir -p ~/.config/newsboat
mkdir -p ~/.config/qutebrowser
mkdir -p ~/.config/ranger
mkdir -p ~/.config/ranger/plugins
mkdir -p ~/.config/ranger/colorschemes
mkdir -p ~/.config/sioyek # experimental as of writing this
mkdir -p ~/.config/sxhkd
mkdir -p ~/.config/zathura

mkdir -p ~/.bin
mkdir -p ~/.vim
mkdir -p ~/.vim/ftplugin
mkdir -p ~/.vim/snips
mkdir -p ~/.cache/vim/swap
mkdir -p ~/.zsh

# Zathura will complain slightly if this doesn't exist
touch ~/.config/zathura/extra_config
