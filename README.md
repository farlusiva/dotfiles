```
       __      __  _____ __
  ____/ /___  / /_/ __(_) /__  _____
 / __  / __ \/ __/ /_/ / / _ \/ ___/
/ /_/ / /_/ / /_/ __/ / /  __(__  )
\__,_/\____/\__/_/ /_/_/\___/____/


  * bin            >  some scripts
  * breeze         >  a theme for Qt and GTK; stow only on Arch
  * bspwm          >  a window manager, includes sxhkd's config
  * cmus           >  the coolest music player in town
  * dunst          >  notification program
  * fastfetch      >  info script
  * feh            >  an image viewer
  * khal           >  a calendar application
  * mpv            >  a media player
  * mutt           >  a cool e-mail client
  * neofetch       >  info script
  * newsboat       >  a scriptable rss/atom reader
  * nixos          >  an OS. Stow with `stow -t /`.
  * picom          >  compton fork. bspwm is prettiest with this on
  * python         >  a programming language
  * qutebrowser    >  internet browsing, one true editor edition
  * ranger         >  file manager
  * scripts        >  scripts used to manage my dotfiles
  * tmux           >  terminal multiplexer
  * urxvt          >  stow with 'stow -t / urxvt'. Also needs X stowed.
  * vim            >  the one true editor
  * X              >  files called `\.[xX].*`
  * zathura        >  a pdf reader with an interface cooler than mupdf's
  * zsh            >  a shell with good completion
```

# My dotfiles

Here are my dotfiles. If you like keyboard-based programs, you may find this of
use. They are managed with GNU stow.

Generally speaking, my "style" is that the software I use the most will be
keyboard-driven (vim, ranger, tmux, qutebrowser) and that the
software I don't use that much (gimp, gparted) will be mouse-driven.
The rationale is that the investment of learning to use keyboard-driven
alternatives won't be worth it for seldom used software.

## Notable features

Over time, my dotfiles have accumulated some features which I think are rare,
or find especially useful. Here are some of them:

* Proper `bspwm` multimonitor keybinds. `alt+{hjkl}` can move focus to other
  monitors, and `alt+shift+{hjkl}` can move windows to other monitors.
* Keybinds (`alt+{io}`) to move to the workspace to the left or right of the
  current one. No need to think about which workspace you are on to move to the
  neighbouring ones!
* Tight `fzf` integration in `zsh`. The centrepiece is a set of keybinds making
  cd-ing to and editing files very quick.
* A file opener using `fzf`, with the same binds as the zsh one.
* A script that does common system actions, notably starting/stopping the
  compositor, turning on/off automatic screen sleeping, pausing/unpausing
  notifications, and mounting/unmounting my cloud disks.
* A system of LaTeX macros in Vim for writing mathematics quickly \[1\].
* A unicode glyph picker.

\[1\]:
Inspired by [Gilles Castel](https://castel.dev/post/lecture-notes-1/),
of course. Another excellent resource is
[this](https://www.ejmastnak.com/tutorials/vim-latex/intro/)
blogpost series.

## Stow

Stow wasn't orignally meant to manage dotfiles like it's used here, but it
works rather well for that purpose, which has lead many people to use stow
to manage their dotfiles. If something here suits your fancy, then install
stow and run __`stow $THING`__, and stow will symlink the necessary files to
their respective locations. Be aware however, that the 'build' folder isn't
meant to be used with stow, as it's used for building st and dmenu.

In order to get urxvt's clipboard working, you'll need to run
__`stow -t / urxvt`__ as root, since the perl script is placed in /usr/lib.

It's also important to note that bspwm will try to start a lot of things
that are stowed individually, such as picom and the Xmodmap (in X).


**Note**: Some programs put stuff in their config folders (such as qutebrowser,
cmus, ranger, vim and more). To avoid polluting the local copy of this git
repo, it is advised to create the config folders *before* running `stow`. This
can be done by running `scripts/mk_conf_dir_structure.sh` before stowing
anything.

## Reproducing it on NixOS

As of writing this, the NixOS configuration is a mix of immutable and
non-immutable configuration. I've done this so as to avoid integrating the
dotfiles with NixOS, which would make it hard(er) to use the dotfiles on other
distros.

To reproduce the setup, you need to

0. Install a minimal NixOS with networking. Install `git` and `stow`. Create
   your (sudoer) user account, and set the password.
1. Clone this repo. Symlink your desired host-file (e.g. `dionysus.nix`) to
   `nixos/etc/nixos/configuration.nix` in this repo's folder. Run `stow -t /
   nixos`.
2. Run `scripts/mk_conf_dir_structure.sh` as the user you'll be stowing as.
   Stow *at least* `bin`, `bspwm` and `X`. If you don't stow `zsh` and set it
   as your shell (see `nixos/users`), then ensure that the `XDG_CONFIG_HOME`
   and `PATH`-stuff handled in the `.zprofile` is done in your `.${SHELL}rc`.
3. Make a patch for `st` that patches NixOS's version of st to use my own
   config and patches; see `build/st`.  Put it in `/etc/nixos/st-patch.diff`.
4. Run `scripts/mk_nix_channels.sh` to set up the unstable channel.
5. Run `nixos-rebuild switch`.

## Dependencies

Run `depcheck.sh` to see if dependencies and the various tools/programs are
installed.

## A note on fonts and branches

Fonts are set by hostname, currently `athena`, `dionysus`, and `hestia`.
Grep for these if you wish to change the fonts sizes.

Before the change that finalized this (done 2019-02-28), branches of this
repo named athena and dionysus were used for their respective boxes, meaning
that I had to maintain multiple branches with few changes between. I didn't
like that so I added conditionals. The old branches, athena and dionysus, are
to be left unused from the date of master superseding them, 2019-02-28.

## Files not in this repo

Not all files that should be present on my different computers are stored in
this repo, and there might be references to these lying around. Some of these
files are:

* My RSS feeds for newsboat
* My SSH keys and KeePass database
* Qutebrowser's `quickmarks`, `bookmarks/urls` and extra config
* My `vdirsyncer` config, which is used by `khal`
* My `rclone` config.
* My `mutt` account profiles.
* My `~/.gitconfig`
* My wallpaper collection
