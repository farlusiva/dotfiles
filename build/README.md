# Build

Here is essentially build scripts for suckless things, as if I want to use them
with my config, then I'd have to build it myself. Because i'm lazy as heck, I
decided to write some build scripts that would pull the git repo, apply patches
(if there are any I want), and compile. Combine that with a simple makefile,
and you have this.

For st, the config was initially written to be as close a match as that of
my urxvt config. The only difference (that I know of) as of writing this is how
the terminal emulators hide your pointer. St hides when you type, urxvt hides
when your pointer is inactive for a second. To un-hide the pointer, on st you'd
need to click, while on urxvt you just move it.
