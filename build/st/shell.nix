# vim: syn=conf

{ pkgs ? import <nixpkgs> {} }:

  pkgs.mkShell {
    buildInputs = with pkgs; [
        # To make st
        pkg-config
        xorg.libX11
        ncurses
        xorg.libXft
        xorg.libXrender

        # For the script and patching st
        gnumake
        wget
        patch
    ];
}
