#!/bin/sh
# Start DBus, which is required on NixOS for some reason.
# This xinitrc does not follow the Xmodmap and Xresources specified in the
# system, i.e. /etc/X11/xinit/.{Xresources,Xmodmap} (or the nixos equivalent).

# Start DBus if on NixOS.
# See https://nixos.wiki/wiki/Using_X_without_a_Display_Manager
if grep -q NixOS /etc/os-release && [ -z "$DBUS_SESSION_BUS_ADDRESS" ]; then
	eval "$(dbus-launch --exit-with-session --sh-syntax)"
fi

# This gets eval'd on Arch as well, as it is in
# /etc/x11/xinit/xinitrc.d/50-systemd-user.sh, which gets sourced in the
# default xinitrc, so it's probably important.
systemctl --user import-environment DISPLAY XAUTHORITY

if command -v dbus-update-activation-environment >/dev/null 2>&1; then
	dbus-update-activation-environment DISPLAY XAUTHORITY
fi


# Start some nice programs.
# Does nothing on NixOS by default, as the distro doesn't place anything there.
# I have placed some files there, however, such as from hestia.nix.
if [ -d /etc/X11/xinit/xinitrc.d ]; then
 for f in /etc/X11/xinit/xinitrc.d/?*.sh; do
  [ -x "$f" ] && . "$f"
 done
 unset f
fi

# setup the monitors on athena
if [ "$(hostname)" = "athena" ]; then
	#PRIM="DP-2"
	PRIM="HDMI-0"
	SEC="DVI-I-1"
	if xrandr | grep -q "^$PRIM connected"; then
		xrandr --output "$PRIM" --auto --primary
	fi
	if xrandr | grep -q "^$SEC connected"; then
		xrandr --output "$SEC" --auto --left-of "$PRIM"
	fi
fi

# Start X
setxkbmap no
xmodmap ~/.Xmodmap
xrdb -load ~/.Xresources
# bspwm doesn't set the cursor; see
# https://wiki.archlinux.org/title/Cursor_themes#Change_X_shaped_default_cursor
# Aviod the default "cross" cursor by setting a cursor manually.
xsetroot -cursor_name left_ptr
exec bspwm
