#!/usr/bin/env sh
#
# Inspiration: https://www.youtube.com/watch?v=zB_3FIGRWRU
#
# Usage: If no -t is given, then it runs in the current terminal. If -t is
# given, then it will try to run it in that terminal.
# E.g. notes-script.sh -t "st -e" -f [...].


NAME="$(basename "$0")"
fname=""
cmd_term=""

usage() {
	echo "usage: $NAME [-h] [-t term] file"
}

# To be configured with an environment variable.
if [ -z "$VIM" ]; then
	VIM=vim
fi

while [ "$#" -ge 1 ]; do
	case $1 in
		-h)
			usage
			shift
			exit
		;;
		-t)
			cmd_term="$2"
			shift 2
		;;
		*)
			fname="$1"
			shift
		;;
	esac
done

if [ -z "$fname" ]; then
	echo "$NAME: no file given"
	exit
fi

VIMSTR=""
DAY="$(date -I)"
TIME="$(date "+%H:%M")"

VIMSTR+=":norm G"

if [ -f "$fname" ]; then
	VIMSTR+="o"

	if ! grep -q "# Notes for $DAY" "$fname"; then
		VIMSTR+="Go# Notes for $DAY"
	fi
fi

$cmd_term sh -c "$VIM -n -c '$VIMSTR' -c 'set ft=markdown' -c 'norm Go# $TIME' \
                      -c 'norm G2o' -c 'norm zz' '$fname'"
