#!/bin/sh
#
# Runs dmenu with the right options and fontsize.

if [ "$(hostname)" = "athena" ]; then
	FONTSIZE=12
elif [ "$(hostname)" = "dionysus" ]; then
	FONTSIZE=11
elif [ "$(hostname)" = "hestia" ]; then
	FONTSIZE=14
else
	FONTSIZE=20
fi

# Integration with pywal
# from https://github.com/dylanaraps/pywal/wiki/Customization
if [ -f ~/.cache/wal/colors.sh ]; then

	source "${HOME}/.cache/wal/colors.sh"

	# But apply a dmenu override, if there is one
	if [ -f "${HOME}/.cache/wal/dmenu_colours.sh" ]; then
		source "${HOME}/.cache/wal/dmenu_colours.sh"
	fi

	dmenu -i -nf "$color15" -nb "$color0" -sf "$color15" -sb "$color1" \
		  -fn "tamzen for Powerline:size=$FONTSIZE" "$@"

else
	# pywal not active

	dmenu -i -nf \#bbbbbb -nb \#202020 -sf \#111111 -sb \#aaaaaa \
		  -fn "tamzen for Powerline:size=$FONTSIZE" "$@"
fi
