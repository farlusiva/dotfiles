#!/bin/sh
#
# A dmenu menu for setting themes
#
# TODO: Enable use of builtin pywal themes such as base16-gruvbox-hard,
# base16-onedark &c.

DMENU="dmenu"
FPATH="" # Filepath for the json files. They're detected automatically.

help_menu() {
	echo "usage: $(basename "$0") [args]"
	echo
	echo "args:"
	echo
	echo "	-d (cmd)	dmenu command; default '$DMENU'"
	echo "	-p (path)	where to look for pywal .json files (must be set to run)"
}

while [ "$#" -gt 0 ]; do
	case $1 in

	-h)
		help_menu
		shift
		exit 0
		;;
	-d)
		DMENU="$2"
		shift
		shift
		;;
	-p)
		FPATH="$2"
		shift
		shift
		;;
	*)
		help_menu
		shift
		exit 1

	esac

done

if [ -z "$FPATH" ]; then
	echo "$0: Must run with -p; exiting."
	exit
fi
if ! [ -d "$FPATH" ]; then
	echo "$0 The filepath must be a directory."
fi

# For when setting themes that don't have a canonical / associated wallpaper.
# From pywal_change.sh.
reset_wall() {
	if [ -f /tmp/wall ]; then
		rm /tmp/wall
		convert -size 10:10 "xc:#2d2d2d" /tmp/wall.png
		mv /tmp/wall.png /tmp/wall
		feh --no-fehbg --bg-fill /tmp/wall;
	fi
}


# Find themes.
# Looks for files matching "wal-(...).json".
# Also has some selected builtin pywal themes.

themes="$({ \
find "$(realpath "$FPATH")" -maxdepth 1 -type f | grep '^.*/wal-.*\.json' \
	| sed -e 's_^.*/__' -e 's_\.json$__'
# dark builtin themes
echo "thm-gruvbox"
echo "thm-sexy-trim-yer-beard"
echo "thm-base16-brewer"
echo "thm-base16-material"
echo "thm-dkeg-owl"
#echo "thm-sexy-tango" # too similar to normal non-pywal theme
# light builtin themes
echo "thml-base16-gruvbox-soft"
}
)"

sel="$(printf "turn off\ncurrent wallpaper\n$themes" | "$DMENU" -p "theme?" )"

if [ -z "$sel" ]; then
	exit
fi

case "$sel" in
	"turn off")
		pywal_change.sh --off
		;;
	"current wallpaper")
		pywal_change.sh --on -i /tmp/wall
		;;
	thm-*)
		sel="$(echo "$sel" | sed "s/^thm-//")"

		# TODO: Add special cases for certain themes,
		#       like telling pywal_change.sh to *not* perform certain
		#       operations (like setting the cmus theme to green)?
		pywal_change.sh --on --theme "$sel"
		# Themes don't have preset walls, so reset (if any)
		reset_wall
		;;
	thml-*)
		sel="$(echo "$sel" | sed "s/^thml-//")"
		pywal_change.sh --on -l --theme "$sel"
		reset_wall
		;;

	wal-*)
		pywal_change.sh --on --theme "$FPATH"/"$sel".json
		;;

	*)
		exit 1
		;;

esac
