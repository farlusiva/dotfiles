#!/bin/sh
#
# Will locally mount my drives.

PREFIX="$HOME"

do_mount() {


	OPTS="--dir-cache-time 48h --daemon"

	if [ -d $PREFIX/gdrive ]; then
		echo "$0: ~/gdrive exists, not attempting to mount it."
	else
		mkdir -p $PREFIX/gdrive || return 1
		rclone mount gdrive:/ $PREFIX/gdrive $OPTS
	fi

	if [ -d $PREFIX/onedrive ]; then
		mkdir -p $PREFIX/onedrive || return 1
		echo "$0: ~/gdrive exists, not attempting to mount it."
	else
		rclone mount onedrive:/ $PREFIX/onedrive $OPTS
	fi


}

do_unmount() {

	if [ -d $PREFIX/gdrive ]; then
		fusermount -u $PREFIX/gdrive || exit 1
		rmdir $PREFIX/gdrive
	else
		echo "$0: No ~/gdrive found, not attempting to unmount."
	fi

	if [ -d $PREFIX/onedrive ]; then
		fusermount -u $PREFIX/onedrive || exit 1
		rmdir $PREFIX/onedrive
	else
		echo "$0: No ~/onedrive found, not attempting to unmount."
	fi
}

info_help() {
	echo "usage: $0 [mount] [unmount]"
}

case $1 in
	mount) do_mount ;;
	unmount) do_unmount ;;
	*) info_help ;;
esac
