#!/usr/bin/env bash
#
# trayd.sh
#
# a tray daemon

# TODO: Investigate some good tray programs (patray?)
# TODO: Find a better place to place the stalonetray, geometrically.
#       See stalonetray -h.
#       Also, give it darkmode.
#       Give it "-display" of the currently focused display, so it can be
#       put on an arbitrary monitor when multimonitor.

# Other tray programs (should I not use stalonetray):
# https://github.com/sargon/trayer-srg

FILE="/tmp/trayd_pid"

# if using bspwm
IN_BSPWM=false


usage() {
	printf "usage: $(basename "$0") [ARG]\n" >&2
	printf "\n" >&2
	printf "Arguments:\n" >&2
	printf "\t-g\tget pid of daemon\n" >&2
	printf "\t-b\ttell bspwm to ignore ewmh struts for the tray\n" >&2
	printf "\t-h\tget help\n" >&2
	printf "\t-k\tkill the daemon\n" >&2
	printf "\t-s\tstart the daemon, and fork it\n" >&2
	printf "\t-S\tstart the daemon, and don't\n" >&2
	printf "\t-t\ttoggle the daemon\n" >&2
}

# exit code says if a daemon is already running
check_daemon() {

	if [ ! -e "$FILE" ]; then
		# does not exist anything there
		return 1

	elif [ -f "$FILE" ]; then
		return 0
	fi

	echo "$0: error when checking existence of daemon, there's something at $FILE!" >&2
	exit 0
}

kill_daemon() {

	if ! check_daemon; then
		return;
	fi

	pid="$(cat "$FILE")"
	echo "Killing $pid"

	# let it handle its own death via the exit trap
	kill "$pid"

}

start_daemon() {

	if check_daemon; then
		echo "$(basename "$0") $*: daemon already running (locked by $FILE); exiting." >&2
		exit 1
	fi

	trap cleanup INT TERM EXIT

	pid="$$"
	echo "$pid" > "$FILE"

	if [ "$IN_BSPWM" = "true" ]; then
		BSP_EWMH_CONF="$(bspc config ignore_ewmh_struts)"
		bspc config ignore_ewmh_struts true
		sleep 0.1
	fi
	# Daemon
	stalonetray -bg "#202020" & # 202020 is same as lemons "back". TODO.
	pid_stalone="$!"

	nm-applet &
	udiskie -t &
	syncthingtray &
	#blueberry-tray & # not used enough to warrant


	echo "$0: Daemon running. My pid is $pid"

	# give stalonetray time to start could be done more carefully by waiting
	# until stalonetray has started by checking with xdotool every 0.1s until
	# 10s have passed.
	if [ "$IN_BSPWM" = "true" ]; then
		sleep 0.25
		bspc config ignore_ewmh_struts "$BSP_EWMH_CONF"
	fi

	# wait specifically until stalonetray dies for itself to quit
	# (thus killing the other daemons as well)
	wait "$pid_stalone"

	# cleanup wil be automatically initiated by the exit trap, so nothing
	# to do here.

}

# Safely cleanup
cleanup() {

	cur_pid="$(cat "$FILE" 2>/dev/null)"
	if [[ "$cur_pid" = "$$" ]]; then
		rm "$FILE"
	fi

	# kill all subprocesses (stalonetray &c.)
	# https://unix.stackexchange.com/a/706789

	{ pgrep -P $$ | xargs kill; } >/dev/null 2>&1

}

start_forked_daemon() {
	THIS_FILE="$(realpath "$0")"

	# Hack
	if [ "$IN_BSPWM" = "true" ]; then
		nohup sh -c "$THIS_FILE -b -S" >/dev/null 2>&1 &
	else
		nohup sh -c "$THIS_FILE -S" >/dev/null 2>&1 &
	fi
}



while (( $# )); do
	case "$1" in

	-h|--help)
		usage
		exit 0
		;;
	-b)
		IN_BSPWM="true"
		shift
		;;
	-k)
		kill_daemon
		exit 0
		;;
	-t)
		# flips between -s and -k
		if check_daemon; then
			kill_daemon
		else
			start_forked_daemon
		fi
		exit 0
		;;
	-s)
		echo "starting forked daemon"
		start_forked_daemon
		exit 0
		;;
	-S)
		start_daemon
		exit 0
		;;
	-g)
		if [ -f "$FILE" ]; then
			cat "$FILE" 2>/dev/null
			exit 0
		fi
		exit 1
		;;
	-*) # also matches '--*'
		usage
		exit 1
		;;
	*)
		usage
		exit 0
		;;
	esac

done

# if no arg was given
usage
exit 1
