#!/bin/sh
#
# Easily connect to/disconnect from your usual bluetooth devices with
# bluetoothctl. Compared to the others (i.e. the one I tried), this one tries
# to require as few dmenu interactions as possible. This makes it less featured
# than some of the others, but also quicker to use.
#
# For the moment, it gives no feedback to the user. Maybe it should do
# something via notify-send?
#
# Some inspiration taken from here: https://github.com/Layerex/dmenu-bluetooth

for k in bluetoothctl awk sed rfkill xargs basename; do
	command -v "$k" > /dev/null || { echo "$k not installed"; exit; }
done

usage() {
	echo "usage: $(basename "$0") [-h|--help] [-d|--dmenu DMENU]"
}


DMENU="dmenu -i"

while [ "$#" -ne 0 ]; do
	case "$1" in

	-d|--dmenu)
		if [[ -n $2 ]]; then
			echo "$@"
			DMENU="$2"
			shift 2
		else
			echo "$(basename "$0"): Argument needed"
			exit 1
		fi
		;;
	-h|--help)
		usage
		exit 0
		;;
	esac
done


is_powered() { bluetoothctl show | grep -Fq "Powered: yes"; }

check_device_conn() { bluetoothctl info "$1" | grep -Fq "Connected: yes"; }

dmenu_namelist_maker() {
	echo "$1" | while read -r k; do
		# Get connectedness status, and add it to the string

		ID="$(echo "$k" | sed -e "s/ .*$//")"
		NAME="$(echo "$k" | sed -e "s/^[0-9A-F:]* //")"

		if check_device_conn "$ID"; then
			echo "$NAME disconnect"
		else
			echo "$NAME connect"
		fi
	done
}


# Turn power on
power_on() {
	# Check if rfkill has blocked us
	if rfkill list bluetooth | grep -Fq "blocked: yes"; then
		rfkill unblock bluetooth
		sleep 0.3
	fi

	# Check if power is on
	if ! is_powered; then
		bluetoothctl power on
		sleep 0.3
	fi
}


main() {

	if is_powered; then
		powerstatus="Bluetooth off"
	else
		powerstatus="Bluetooth on"
	fi

	FULL_DEVICES="$(bluetoothctl devices | sed -e "s/^Device //")"
	DEVICES="$(dmenu_namelist_maker "${FULL_DEVICES}")" # Make pretty list
	string="\n$powerstatus\n$DEVICES"
	choice="$(printf "$string" | $DMENU -p "bluetooth")"

	case "$choice" in
		"Bluetooth on")  bluetoothctl power on ;;
		"Bluetooth off") bluetoothctl power off ;;
		"") ;;

		*)

			if [ "$choice" = "" ]; then
				return
			fi

			# Cut off 'connect/disconnect' part at the end
			choice="$(echo "$choice" | sed -e 's/ \(dis\)\?connect$//')"

			# Hope the names map uniquely to IDs. In any case, do our best.
			ID="$(echo "${FULL_DEVICES}" | grep -F "$choice" | awk '{print $1; exit; }')"

			# Toggle connecting/disconnecting

			if check_device_conn "$ID"; then
				bluetoothctl disconnect "$ID"

				sleep 3

				# If no other devices are connected, turn off bluetooth.

				if ! echo "${FULL_DEVICES}" | awk '{print $1}' | \
				       xargs -n 1 bluetoothctl info 2>/dev/null | \
				       grep -Fq "Connected: yes"; then

					bluetoothctl power off
				fi


			else
				if ! is_powered; then
					power_on
				fi

				bluetoothctl connect "$ID"
			fi

		;;
	esac
}

main
