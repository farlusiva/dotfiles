#!/bin/sh


help_menu() {
	echo "usage: $0: --on [ARGS TO WAL] OR --off"
	echo "pywal is called with the given args, usually -i FILE or --theme THEME."
}


# Requires the existence of the wal files in ~/.cache
post_wal() {
	. "${HOME}/.cache/wal/colors.sh"

	# Do things that should be done after pywal has been ran.

	# Restart lemonbar
	if ~/.config/bspwm/lemond.sh -g; then
		~/.config/bspwm/lemond.sh -t
		~/.config/bspwm/lemond.sh -t
	fi

	# bspwm.
	# From here: https://github.com/dylanaraps/pywal/wiki/Customization#bspwm
	# Set the border colors.
	bspc config normal_border_color "$color8"
	bspc config active_border_color "$color2"
	bspc config focused_border_color "$color15"
	bspc config presel_feedback_color "$color4" # req. bspwm >= v0.9.4



	# Change the dunst colours
	# Dunst, since v1.8.0 has a "~/.config/dunst/dunstrc.d/" folder
	# where configs apply *after* the main dunstrc, so we can place
	# a file in there and override the colours in our dusntrc.

	cat > ~/.config/dunst/dunstrc.d/10-pywal.conf << EOF
[global]
	frame_color = "$color4"
	separator_color = frame
[urgency_low]
	background = "$background"
	foreground = "$foreground"
[urgency_normal]
	background = "$background"
	foreground = "$foreground"
[urgency_critical]
	background = "$background"
	foreground = "$foreground"
EOF

	pkill dunst
	# dunst will startup next time notify-send is called, so we don't need
	# to think about that, unless there are font problems.

	# st
	# Uses the "xresources reload signal" patch here:
	# https://st.suckless.org/patches/xresources-with-reload-signal/
	# Remember to call wal with -s to avoid changing the terminal colours
	# twice.
	xrdb ~/.cache/wal/colors.Xresources
	pidof st | xargs kill -s USR1
	# The line above will somehow give every st(1) window the X11 "urgent" flag,
	# so remove them by making them un-urgent.
	#xdotool search --class "st-256color" set_window --urgency 0 "%@"
	# TODO: Make it work, it seems that bspwm won't unset the urgent flag if
	#       it is removed via xdotool?


	# A scheme with some colours, and thus better for when pywal is in use.
	# Should pywal be active when cmus is starting, then it'll set its own
	# colourscheme accordingly. Hence, here we only need to consider the case
	# of when cmus is currently running.
	cmus-remote -C "colorscheme green"

	# Zathura
	# TODO: See if a homemade zathura plugin can tell zathura to dynamically
	# change its colourscheme like ranger has been made to.
	cat << EOF > ~/.config/zathura/extra_config
set default-bg   "$background"
set default-fg   "$foreground"

set inputbar-bg  "$background"
set inputbar-fg  "$foreground"

set statusbar-bg "$background"
set statusbar-fg "$foreground"

set recolor-lightcolor "$background"
set recolor-darkcolor "$foreground"

set index-bg "$background"
set index-fg "$foreground"
set index-active-fg "$color0"
set index-active-bg "$color2"

set completion-bg "$background"
set completion-fg "$foreground"
set completion-highlight-bg "$color2"

EOF

}

go_back() {

	# Restart lemonbar
	if ~/.config/bspwm/lemond.sh -g; then
		~/.config/bspwm/lemond.sh -t
		~/.config/bspwm/lemond.sh -t
	fi

	# copied from the bspwmrc, and from bspwm's defaults
	bspc config normal_border_color "#30302f"
	bspc config active_border_color "#474645"
	bspc config focused_border_color "#817f7f"
	bspc config presel_feedback_color "#817f7f"

	# Set the background back to normal (copied from the bspwmrc)
	rm /tmp/wall
	convert -size 10:10 "xc:#2d2d2d" /tmp/wall.png
	mv /tmp/wall.png /tmp/wall
	feh --no-fehbg --bg-fill /tmp/wall;

	# Change the st theme (if possible?) on the fly

	# Make dunst go back
	rm ~/.config/dunst/dunstrc.d/10-pywal.conf
	pkill dunst

	# st
	xrdb ~/.Xresources
	pidof st | xargs kill -s USR1

	cmus-remote -C "colorscheme dark"

	echo "" > ~/.config/zathura/extra_config

}

if [ "$1" = "--on" ]; then

	shift
	args="$@"

	if [ -z "$args" ]; then
		echo "$0: need args to give to wal"
		exit 0
	fi

	echo "$0: Going wal on args $@"

	wal -t -s $args && post_wal


elif [ "$1" = "--off" ]; then

	echo "$0: Going back to non-wal setup"
	rm -rf ~/.cache/wal
	go_back

else
	help_menu
	exit 1

fi
