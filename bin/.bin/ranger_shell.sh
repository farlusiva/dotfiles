#!/bin/sh
#
# When ranger is exited with 'q', then it will open the shell and cd into it.

for k in ranger zsh; do
	command -v "$k" > /dev/null 2>&1 ||
		{ echo "$k not found; exiting"; exit 1; }
done


# From examples/shell_automatic_cd.sh in the ranger repo.
ranger_cd() {
    temp_file="$(mktemp -t "ranger_cd.XXXXXXXXXX")"
	trap "rm -f -- '$temp_file'" EXIT
    ranger --choosedir="$temp_file" "$@"
    if chosen_dir="$(cat -- "$temp_file")" && [ -n "$chosen_dir" ] && [ "$chosen_dir" != "$PWD" ]; then
        cd -- "$chosen_dir" || exit
    fi
    rm -f -- "$temp_file"
}

ranger_cd "$@"
NO_FETCH=0 zsh -i
