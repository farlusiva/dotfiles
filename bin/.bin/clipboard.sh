#!/bin/sh
#
# Lets the user do things with the contents in their clipboard. It currently
# only lets them specify commands to run (such as mpv, youtube-dl, wget), but
# it could be expanded to do more, such as having a mass-downloader if would
# use it often enough to warrant adding it.


CLIPBOARD_COMMAND="xclip -o -selection clipboard"
DMENU="dmenu"
EDITOR="$VISUAL"
MPV_FLAGS="--force-window=immediate"
TERMINAL="st -e"

for k in nohup basename mktemp dirname notify-send cut curl zathura; do
	command -v "$k" > /dev/null || { echo "$k not installed"; exit; }
done

NAME="$(basename "$0")"

# Incase the user runs youtube-dl or something similar.
cd ~ || { echo "$NAME: Couldn't cd to ~; exiting."; exit 0; }


help_menu() {
	echo "usage: $NAME [args]"
	echo
	echo "args:"
	echo
	echo "  -d (cmd)    dmenu command; default '$DMENU'"
	echo "  -e (cmd)    editor; default \$VISUAL"
	echo "  -h          display this message"
	echo "  -c (cmd)    set the clipboard command; default '$CLIPBOARD_COMMAND'"
	echo "  -t (cmd)    set the terminal command; default '$TERMINAL'"
	echo "  -m (flags)  mpv flags; default '$MPV_FLAGS'"
}

# Gets the screen resolution the most pixels, for use in graphical mpv.
max_screen_res() {

	if ! command -v xrandr >/dev/null; then
		echo "1920x1080"
		return
	fi

	xrandr | awk '/*/ {print $1}' | awk -F x '
	BEGIN { res = ""; high = 0 }
	{
		if ($1 * $2 > high) {
			res = $1"x"$2
			high = $1 * $2
		}
	}
	END { print res }
	'
}

while [ "$#" -gt 0 ]; do
	case $1 in
	-c)
		CLIPBOARD_COMMAND="$2"
		shift
		shift
		;;
	-d)
		DMENU="$2"
		shift
		shift
		;;
	-e)
		EDITOR="$2"
		shift
		shift
		;;
	-h)
		help_menu
		shift
		exit 0
		;;
	-t)
		TERMINAL="$2"
		shift
		shift
		;;
	*)
		help_menu
		shift
		exit 1
	esac
done


if [ -z "$EDITOR" ]; then
	echo "$NAME: No editor given!"
	exit 1
fi

CLIP="$($CLIPBOARD_COMMAND)"

if [ -z "$CLIP" ]; then
	notify-send -a clipboard.sh -u low "Clipboard empty."
	exit 1
fi

CMD="$(
	# Note that this also takes in other commands
	printf "edit\nmpv\nmpvaudio\nyoutube-dl\nwget\ncurl\nopen as pdf\n" | \
		$DMENU -p "What to do with the clipboard?"
)"

if [ -z "$CMD" ]; then
	exit 1
fi

case $CMD in

	edit)
		FILE="$(mktemp)"
		trap "rm '$FILE'" EXIT
		printf "%s" "$CLIP" > "$FILE"
		nohup $TERMINAL sh -c "cat '$FILE' | $EDITOR -" >/dev/null 2>&1 &

		sleep 5 # Give the editor time to start
		rm "$FILE"
		exit 0
		;;

	mpv)
		height="$(max_screen_res | cut -d x -f 1)"
		MPV_FLAGS+=" --ytdl-raw-options=format=bestvideo[height<=$height]+bestaudio/best[height<=$height]"
		mpv $MPV_FLAGS -- "$CLIP"
		exit 0
		;;
	mpvaudio)
		nohup $TERMINAL mpv -vo=null "$MPV_FLAGS" -- "$CLIP" >/dev/null 2>&1 &
		exit 0
		;;
	"open as pdf")
		# Makes it possible to avoid the default firefox pdf viewer when I
		# don't want to use it.
		# TODO: A future expansion is to make it two options in dmenu,
		# (i) the first one uses whatever rifle recommends to open the file,
		# and (ii) opening with zathura.
		# Code for (i) can be copied from file_opener.sh.
		FNAME="/tmp/$(echo "$CLIP" | awk -F/ '{print $NF}')"
		if ! [ -f "$FNAME" ]; then curl "$CLIP" -o "$FNAME" || exit 0; fi
		zathura "$FNAME" 2>&1 &
		exit 0
		;;
	*)
		$CMD "$CLIP" || { notify-send -a clipboard.sh "Command failed."; }
	;;
esac
