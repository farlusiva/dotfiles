#!/usr/bin/env bash
#
# Used to connect / make profiles to enterprise networks, which nmtui cannot.
#
# It starts stalonetray to give the applet a place to be, and then runs nm-applet.


stalonetray &
nm-applet &

# Only stop the script when the children are finished.
wait
wait
