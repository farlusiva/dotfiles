#!/bin/sh
#
# A program to open, edit and cd to a file.
#
# This file is the third in line of my programs to open files. The first,
# dot_edit.sh, existed for a few months in 2019. The second, project_edit.sh,
# replaced dot_edit.sh and was in use until late 2021. Both were only meant to
# open/edit my dotfiles.
#
# Run with -t if the terminal running this shall run the filer picker. If not,
# the script will open its own terminal to do this.

# Note: Currently, usage of st(1) and bspc(1) are hardcoded. So is the folder
# that fd searches in (which is $HOME).

FOLDER="$HOME"

# Default options
IS_TERM=0
MUL_INST=0
DEF_ACT="ctrl-m" # default action; ctrl-m is "use whatever rifle reccommends".


# Give fzf some nicer colours. From the zshrc.
export FZF_DEFAULT_OPTS="
    $FZF_DEFAULT_OPTS --no-color
	--color bg:-1,bg+:-1,fg+:-1,fg+:regular,hl+:8,hl:8,hl:regular,hl+:regular
"

help_menu() {

	NAME="$(basename "$0")"
	echo "usage: $NAME [args]"
	echo "args:"
	echo
	echo "  -h    display this message"
	echo "  -m    allow multiple instances"
	echo "  -t    run fzf in the terminal running this"
	echo "  -a    set default action (ctrl-{m,e,k,t,r})"
}

while [ "$#" -gt 0 ]; do
	case $1 in

	-h)
		help_menu
		shift
		exit 0
		;;
	-t)
		IS_TERM=1
		shift
		;;
	-a)
		DEF_ACT="$2"
		shift 2
		;;
	-m)
		MUL_INST=1
		shift
		;;
	*)
		help_menu
		shift
		exit 1
		;;

	esac

done


# ctrl-e: edit in $EDITOR and cd to the folder in the interactive shell
# ctrl-k: cd to the folder containing the file
# ctrl-t: edit in $EDITOR, without cding
# ctrl-r: open in ranger (also ctrl-o)
#  enter: open with rifle(1)

if [ "$IS_TERM" = "1" ]; then
	# This part of the script is ran by the terminal
	# Note the --one-file-system, used for performance for me, since I keep my
	# cloud mounted drives in ~ when they're mounted.
	FD_COMMAND="fd . $FOLDER -I --hidden -E .git -E .cache -E .stversions"
	sel="$($FD_COMMAND -t f -t l -0 --one-file-system | fzf -m --read0 --expect=ctrl-t,ctrl-e,ctrl-k,ctrl-r,ctrl-o,ctrl-j --tiebreak=length)"

	press="$(echo "$sel" | head -n 1)"
	sel="$(echo "$sel" | tail -n +2)"


	# Hide the terminal from the user. This makes the terminal emulator
	# dissapear from the user's view before it would have if st would close itself
	# after finishing this script.
	if [ "$press" != "ctrl-j" ]; then
		bspc node focused --flag hidden
	fi

	# If "press" is empty, set it to the "default action".
	if [ "$press" = "" ]; then
		press="$DEF_ACT"
	fi


	echo "$sel" | while read -r FILE; do

		FILEPATH="$(dirname "$FILE")"

		if [ -z "$FILE" ]; then
			exit
		fi

		case "$press" in

		ctrl-m|ctrl-j)
			# ctrl-m: the default: open with the default program.
			# ctrl-j: ask what to open the file with.

			# Parse rifle's output so we can fork the terminal-requiring programs by
			# ourselves. Those with a 'f' flag are forked without a terminal, and
			# those without are given a terminal.

			# Below we select which line of "rifle -l" output should be used.
			if [ "$press" = "ctrl-m" ]; then
				RIFOUT="$(rifle -l "$FILE" | sed "/^[0-9]*:::ask/d" | head -n 1)"

			elif [ "$press" = "ctrl-j" ]; then
				# Code exclusive to ctrl-j.
				RIFOUT="$(rifle -l "$FILE" | sed "/^[0-9]*:::ask/d")"
				SEL_RIF="$(echo "$RIFOUT" | awk -F: '{print $4}' | awk '{print $1}' | fzf -m +s)"
				if [ "$SEL_RIF" = "" ]; then
					exit
				fi
				RIFOUT="$(echo "$RIFOUT" | grep -m 1 "$SEL_RIF" -)"
			fi

			if [ "$(echo "$RIFOUT" | awk -F: '{print $3}')" = "f" ]; then
				FORK=1
			else
				# Assume it's supposed to be ran in a terminal.
				FORK=0
			fi

			CMD="$(echo "$RIFOUT" | awk \
									-v EDITOR="$EDITOR" \
									-v VISUAL="$VISUAL" \
									-v PAGER="$PAGER" \
			-F: \
				'{
					gsub("\\$EDITOR", EDITOR, $4)
					gsub("\\$VISUAL", VISUAL, $4)
					gsub("\\$PAGER", PAGER, $4)
					print $4
				}')"

			# Replace "$@" with $@ because escaped filenames shouldn't be
			# inside quotes.
			CMD="${CMD/\"\$\@\"/\$\@}"
			# Replace $@ with escaped filename
			CMD="${CMD/\$\@/"$(printf "%q" "$FILE")"}"

			# Remember to cd as well!
			CMD="$(printf "cd %q; %s" "$FILEPATH" "$CMD")"
			;;

		ctrl-e) # edit

			FORK=0
			CMD="$(printf 'cd %q; %s -- %q' "$FILEPATH" "$EDITOR" "$FILE")"
			;;

		ctrl-k) # only cd

			FORK=0
			CMD="$(printf 'cd %q' "$FILEPATH")"
			;;

		ctrl-t) # edit, no cd

			FORK=0
			CMD="$(printf '%s -- %q' "$EDITOR" "$FILE")"
			;;

		ctrl-r|ctrl-o) # open in ranger

			FORK=0
			CMD="$(printf 'ranger_shell.sh --selectfile=%q' "$FILE")"
			;;

		esac

		if [ "$FORK" -eq 1 ]; then
			nohup sh -c "$CMD" >/dev/null 2>&1 &
		else
			nohup st -e sh -c "$CMD; export NO_FETCH=1; $SHELL" >/dev/null 2>&1 &
		fi

		# We need a sleep (for some reason), but it doesn't seem that sleeping is
		# an integral part of why. TODO: Find out why.
		sleep 0.05
	done

else

	if pid="$(xdotool search --class file_opener.sh)" && \
	           [ "$MUL_INST" = "0" ]; then

		xdotool windowactivate "$pid"
		exit
	fi

	bspc rule -a file_opener.sh --one-shot state=floating sticky=on
	st -g 80x10 -c file_opener.sh -e sh -c "$(realpath $0) -t -a '$DEF_ACT'"
fi
