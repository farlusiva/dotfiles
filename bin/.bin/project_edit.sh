#!/bin/sh
#
# A script that uses dmenu to open files in a 'project'.
# Notice: This script has been deprecated in favour of file_opener.sh, and is
#         no longer used.
#
# It would be nicer with bashisms, but I'm keeping this script posixly correct.
#
# Accepts a list of newline-separated files in ~/.projectedit_projects, where
# empty lines and lines beginning with a hash (a '#') are ignored.
#
# NOTE: While I've done some work to make this program usable on other people's
#       computers, it's still not done. This is because there still are some
#       parts of this program that were meant to work nicely with my machine and
#       my preferences. These include i) using st(1) and ii) hardcoding find to
#       ignore git and cache files (which would cause a performance hit if they
#       were to be filtered out by grep, e.g. with the -e flag.

# Example call (previously used in my dotfiles before file_opener took over):
# project_edit.sh -d "dmenu_wrapper.sh -l 5" -e license -e '.diff$' -e "build/st/st" -e "build/st/patches"



PROJECTS_FILE=~/.projectedit_projects
EXCLUDE=""
DMENU="dmenu -l 5"
IS_CD=0
CONT=1

if [ -z "$VISUAL" ]; then
	echo "$0: \$VISUAL is unset or empty; aborting"
	exit 1
fi

for k in sed grep find basename realpath sort sh; do
	command -v "$k" > /dev/null || { echo "$k not installed"; exit; }
done


help_menu() {
	echo "usage: $(basename "$0") [args]"
	echo
	echo "args:"
	echo
	echo "  -d (cmd)    dmenu command; default '$DMENU'"
	echo "  -e (regex)  add case-insensitive regex(es) to exclude; default none"
	echo "  -h          display this message"
	echo "  -i (file)   give the file to parse; default ~/.projectedit_projects"
}

# Parse args

while [ "$#" -gt 0 ]; do
	case $1 in

	-h)
		help_menu
		shift
		exit 0
		;;
	-d)
		DMENU="$2"
		shift
		shift
		;;
	-e)
		EXCLUDE="${EXCLUDE:+${EXCLUDE}\|}$2"
		shift
		shift
		;;
	-i)
		PROJECTS_FILE="$2"
		shift
		shift
		;;
	*)
		help_menu
		shift
		exit 1

	esac

done


# Get the projects
PROJECTS="$(sed -e "s_~_${HOME}_g" -e "/^#\|^$/d" "$PROJECTS_FILE")"

# Do this once for better performance.
FILE_CANDIDATES="$(
	for k in $PROJECTS; do

		cd "$k" 2>/dev/null || continue

		find "$k" -name '.git' -prune -o \( -type d -path '*cache*' \) -prune -o \
		     -type f -exec realpath --relative-base="$HOME" {} +

	done | sed 's_^\([^/]\)_~/\1_g'
)"

if [ -n "$EXCLUDE" ]; then
	FILE_CANDIDATES="$(echo "$FILE_CANDIDATES" | grep -vi "$EXCLUDE")"
fi


while [ "$CONT" -eq 1 ]; do

	# Emulate a do-while loop where the condition is false unless explicitly
	# set to true.
	CONT=0

	CHOICES="$(
		{
			if [ "$IS_CD" -eq 0 ]; then
				echo "cd"
			fi
			echo "$FILE_CANDIDATES"
		} | sort | sh -c "$DMENU"
	)"


	# If 'cd', then try again (without the cd entry)
	# There are two ways to use cd. The one is with a ctrl-return, and the
	# other is by selecting 'cd' first and then the file in the next call of
	# dmenu.
	if [ "$CHOICES" = "cd" ]; then
		IS_CD=1
		CONT=1
		VISUAL=true
	elif echo "$CHOICES" | grep -q '^cd$'; then
		CHOICES="$(echo "$CHOICES" | grep -v '^cd$')"
		VISUAL=true
	fi

done

if [ "$CHOICES" = "cd" ]; then
	exit 0
fi


echo "$CHOICES" | while read -r k; do
	FILE="$(echo "$k" | sed "s_~_${HOME}_g")"
	FILEPATH="$(dirname "$FILE")"

	[ ! -f "$FILE" ] && exit

	nohup st -e sh -c "cd '$FILEPATH'; $VISUAL -- '$FILE'; export NO_FETCH=1; $SHELL" >/dev/null 2>&1 &
done
