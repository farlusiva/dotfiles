#!/bin/sh
#
# Beige theme
#
# Adapted from black_white.sh, without comments.


# Override certain colours for the bar and dmenu
# (Do before pywal_change so the bar sees it when restarting)
mkdir -p ~/.cache/wal
cat << 'EOF' > ~/.cache/wal/lemons_colours.sh
color1='#2f2d28' # pastel darken 0.3 '#827d6e'
EOF
cat << 'EOF' > ~/.cache/wal/dmenu_colours.sh
color1='#2f2d28'
EOF


# TODO: When this gets a gui, find a way to give a theme "alternate
# wallpapers".
#pywal_change.sh --on -i ~/wallpapers/gruvbox_spac.jpg
pywal_change.sh --on --theme ~/dotfiles/wal/beige.json

# Set the wallpaper to be persistent (same level as the other pywal stuff)
cp ~/wallpapers/gruvbox_spac.jpg ~/.cache/wal/wall

# Green is the default cmus colour for when it sees pywal,
# so only remove any other scheme file that might be there.
cmus-remote -C "colorscheme green"
rm -f ~/.config/cmus/scheme_override

# Use the 'dracula' theme because it follows the terminals own colours
echo "dracula" > ~/.config/ranger/extra

for k in /tmp/ranger-ipc.*; do
	echo "set_colourscheme" > "$k" &
done
