#!/bin/sh
#
# Forest theme

# Override certain colours for the bar and dmenu
# (Do before pywal_change so the bar sees it when restarting)
mkdir -p ~/.cache/wal
cat << 'EOF' > ~/.cache/wal/lemons_colours.sh
color1='#3c2518' # pastel darken 0.2 '#845336'
EOF
cat << 'EOF' > ~/.cache/wal/dmenu_colours.sh
color1='#3c2518'
EOF

pywal_change.sh --on --theme sexy-trim-yer-beard

# Set the wallpaper to be persistent (same level as the other pywal stuff)
# We manually set it here also because pywal_change doesn't set it here.
cp ~/wallpapers/forest.jpg ~/.cache/wal/wall
cp ~/.cache/wal/wall /tmp/wall
feh --no-fehbg --bg-fill /tmp/wall;


# so only remove any other scheme file that might be there.
cmus-remote -C "colorscheme green"
rm -f ~/.config/cmus/scheme_override

# Use the 'dracula' theme because it follows the terminals own colours
echo "dracula" > ~/.config/ranger/extra

for k in /tmp/ranger-ipc.*; do
	echo "set_colourscheme" > "$k" &
done
