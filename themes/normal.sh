#!/bin/sh

# This activates the normal theme.

# Normal cleanup activities:


# Remove override for ranger and cmus colourscheme
rm -f ~/.config/ranger/extra
rm -f ~/.config/cmus/scheme_override



# Turn all off
pywal_change.sh --off


# Change ranger colourscheme accordingly
for k in /tmp/ranger-ipc.*; do
	echo "set_colourscheme" > "$k" &
done
