# What's this?

Here are themes that are more extensive than those set by
`theme_setter.sh`/`pywal_change.sh`,
which both only handle pywal themes.
The themes here are supposed to be fully fledged rices by themselves.

For the moment, this is a highly work in progress project.
In the future, the `theme_setter` and pywal stuff in `wal` might be merged
with this.
