#!/bin/sh
#
# Mars theme
#
# Adapted from black_white.sh, without comments.


# Override certain colours for the bar and dmenu
# (Do before pywal_change so the bar sees it when restarting)
mkdir -p ~/.cache/wal
cat << 'EOF' > ~/.cache/wal/lemons_colours.sh
#color1='#28434f' # color2
color1='#1f353e' # pastel darken 0.05 '#28434f' # (color2)
color2='#a4553e' # urgent colour (color5)
EOF
cat << 'EOF' > ~/.cache/wal/dmenu_colours.sh
#color1='#28434f' # color2
color1='#1f353e' # pastel darken 0.05 '#28434f' # (color2)
color2='#a4553e' # urgent colour (color5)
EOF


pywal_change.sh --on --theme ~/dotfiles/wal/wal-mars.json



# Set the wallpaper to be persistent (same level as the other pywal stuff)
cp ~/wallpapers/mars.jpg ~/.cache/wal/wall

# Green is the default cmus colour for when it sees pywal,
# so only remove any other scheme file that might be there.
cmus-remote -C "colorscheme green"
echo "green" > ~/.config/cmus/scheme_override

# Use the 'dracula' theme because it follows the terminals own colours
echo "dracula" > ~/.config/ranger/extra

for k in /tmp/ranger-ipc.*; do
	echo "set_colourscheme" > "$k" &
done
