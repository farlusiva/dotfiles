#!/bin/sh
# Dark theme

# Note: These scripts are supposed to persistently change the config.

# Idea: Here goes config that isn't "general" and can override stuff that
# pywal_change does.
# This is used to create a more integrated, nice theme than can be done
# with the very general script theme_setter/pywal_change.sh.

# This one is a dark mode script.

# This one is persistent.
pywal_change.sh --on --theme dkeg-owl

# Fix the cmus remote command given by pywal_change.sh, which doesn't
# work nicely with this theme.
cmus-remote -C "colorscheme dark"
echo "dark" > ~/.config/cmus/scheme_override


# Override the st terminal config by manually changing the Xresources
# (add if needed)


# Change a part of the bspwm colourscheme
bspc config focused_border_color "#817f7f"
cat << 'EOF' > ~/.cache/wal/bspwm_colours.sh
color15="#817f7f"
EOF


# TODO: Change qutebrowser's colourscheme to fit the theme.
# Also, find a way to "signal" to it to do something.
# This is maybe impossible without a qutebrowser plugin.



# Set a nice wallpaper
# TODO
# For the moment, we set the usual wallpaper because I don't have any better
# idea of what to use.
rm /tmp/wall
convert -size 10:10 "xc:#2d2d2d" /tmp/wall.png
mv /tmp/wall.png /tmp/wall
feh --no-fehbg --bg-fill /tmp/wall;



# Tell ranger to persistently use the 'snow' theme
echo "snow" > ~/.config/ranger/extra

# Also, tell existing rangers to switch theme
# Uses the ranger ipc plugin
# ( https://github.com/ranger/ranger/blob/master/examples/plugin_ipc.py )
for k in /tmp/ranger-ipc.*; do
	echo "set_colourscheme" > "$k" &
done
#for k in /tmp/ranger-ipc.[0-9]*; do echo "set colorscheme snow" > "$k" done
#echo "set colorscheme snow" > /tmp/ranger-ipc.*

# Give 'delta' (the git diffing program) a new theme
# TODO
