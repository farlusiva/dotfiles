# Technical details

Here is some documentation on the state of these dotfiles and their workings.

- As mentioned in the readme, fonts are set by the hostname. The code here
  checks, in multiple places, for the hostnames `athena`, `dionysus` and
  `hestia`. If none of these hostnames are found to be used, the usual
  response is to set a large font size.

- Non-modularity.
  The code is written assuming that I am the user, so the parts are not
  (necessarily) written to be used in isolation. Modularity is not
  a(n absolute) requirement. This allows for hardcoding in some places where
  I couldn't be bothered to avoid it or where it is convenient, such
  as as `file_opener.sh` hardcoding use of `st(1)` and `bspc(1)`. Another
  example of this is that code blindly assumes that the contents of
  `bin/.bin` is in the `PATH`, which e.g. `dmenu_sysutils.sh` and the
  `sxhkdrc` do.

  Multiple scripts assume/hardcode that the lemonbar FIFO pipe is at
  `/tmp/lemons`, such as in `update_bar.sh` in the `cmus` folder,
  and `lemond.sh`.

  The standard location for the current wallpaper is furthermore
  `/tmp/wall`, and this is assumed multiple places (as of writing, the
  `bspwmrc`, the ranger `rc.conf`, and `pywal_change.sh`).

- Code may assume that this repo is in `~/dotfiles`, e.g. the `sxhkdrc`.

- Some of the code here is deprecated and no longer in use, but still lying
  around, or are code lying around from unfinished projects/experiments.
  For example, the first category contains `project_edit.sh` and
  `vim-plugins.sh`, and the second `iso.nix`.

# Known issues / problems

- When `pywal_change.sh` sends a `USR1` signal to every instance of `st(1)`,
  these seem to get the X11 urgent flag for some reason. This is propagated
  over to bar, which is distracting.

- Sometimes, after changing pywal themes, some instances of `st(1)`
  become unresponsive and die after being sent another `USR1` signal.

- The `caffeine` package on NixOS doesn't match with the one on Arch, making
  the nosleep part of `dmenu_sysutils.sh` not work on NixOS.

- Killing lemons from `lemond.sh` doesn't make lemons properly clean its temp
  file in `/tmp`, polluting it with ~1k memory each time lemond restarts lemons.
