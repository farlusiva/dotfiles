" tex.vim

" Hardwrap text to fit 80 chars per line.
setlocal textwidth=80
" Experimental. Looks nicer.
setlocal colorcolumn=

let g:tex_comment_nospell=1

" Using : on the vnoremap is better than <Cmd>'<,'> it seems, so I'll be using
" that instead.
nnoremap <silent> <Leader>l <Cmd>VimtexCountWords<CR>
vnoremap <silent> <Leader>l :VimtexCountWords<CR>


" Makes '-' a part of words.
" This lets us autocomplete latex labels that contain dashes.
" (Note: It also makes ^W delete over them, unlike the default behaviour.)
setlocal iskeyword+=-
