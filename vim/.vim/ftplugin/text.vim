" text.vim
"
" Text width settings are to be 72, with no colorcolumn. This is to make vim
" nicer to use on 80x24 terminal emulators.

setlocal colorcolumn=
setlocal textwidth=72
