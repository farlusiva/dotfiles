" gitcommit.vim
"
" Make it easier to adhere to the 50/72-rule

setlocal ruler
setlocal colorcolumn=72
